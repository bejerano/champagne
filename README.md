# CHAMPAGNE

All evidence listed in the manuscript is located in the _paup-evidence/_ directory. Users can skip to **Step 4** if they wish to use the matrices to infer phylogenies using PAUP\*.

Below, we provide instructions on how to generate a sample character matrix using human (hg38) as the reference genome with dog (canFam3), cat (felCat8), and pig (susScr3) as the query species.

Champagne uses python2, python3, and bash scripts. The script _phyloPipe.py_ creates a joblist to be executed on a [Parasol parallel batch system](https://genecats.gi.ucsc.edu/eng/parasol.html). Users without Parasol can run phyloPipe.py with the --no-parasol (-np) flag to instead run jobs sequentially.

Champagne takes as input:

- For the reference species, an Ensembl gene set is required: [reference]\_genes.byID.bed
- For each query species, the following files are required: [query].fa, [query].2bit, [query].chrom.sizes
- Additionally, for each reference-query species pair, the following files are required: [reference].[query].all.chain, [query].trimmed.chain_ids
- A scoring matrix is required. This paper used the [HoxD55 matrix](http://genomewiki.ucsc.edu/index.php/Hg19_conservation_lastz_parameters).
- A file that maps assembly name to a common species name is used: namemap.txt

---
Some preprocessing is required to generate Champagne's input files:

Generate the reference gene set [reference]\_genes.byID.bed by downloading the Ensembl gene set with the protein\_coding filter and the following fields: Gene ID, Chromosome, Gene Start, Gene End. The file may have to be modified in order to have the 'chr' prefix added to the start of the values in the Chromosome field. Ensembl 86 was used for all matrices created in the paper.

Download each of the query species files from http://hgdownload.cse.ucsc.edu, or generate them using a soft-masked genome assembly.

The reference-query pairwise alignment, or chain file, may be available at http://hgdownload.cse.ucsc.edu or it can be generated from soft-masked genome assemblies using [doBlastzChainNet.pl](https://github.com/ENCODE-DCC/kentUtils/blob/master/src/hg/utils/automation/doBlastzChainNet.pl).

The [query].trimmed.chain_ids file is generated using the scripts found in _chain\_ids\_generation_.

1. From the Ensembl Biomart, download the reference species gene set (as [reference]\_exons.txt) with the protein\_coding filter and the following fields: Gene ID, Transcript ID, Chromosome, Exon Start, Exon End. Please note this is a separate file from what Champagne will use as input.

2. Use _get\_gene\_dict.py_ to generate [reference]\_genes\_chr.p, [reference]\_genes\_coord.p, and [reference]\_canonical\_ids.txt (through stdout).
`
python2 get_gene_dict.py > hg38_canonical_ids.txt
`

3. Use _pickChains.py_ to generate the intermediary .out file. Make sure [reference].[query].all.chain (or a symlink) is in _chain\_ids\_generation/chains/_.
`
python2 pickChains.py hg38 canFam3
`

4. Use _convert\_out\_to\_chainid.sh_ to generate the [query].chain_ids file.
`
./convert_out_to_chainids.sh hg38 canFam3
`

5. Use _trimChainIds.sh_ to conver the [query].chain_ids files to [query].trimmed.chain_ids files.
`
./trimChainIds.sh hg38 canFam3
`

The reference gene set and the scoring matrix should be placed in _inputs/_. 

Each reference and query species needs its own directory (eg. canFam3/) within an overall species directory (_species/_). Each species directory should hold the relevant .fa, .2bit, and .chrom.sizes files. For query species, a subdirectory called ref-[reference] (eg. ref-hg38) should hold the chain file and chain_ids file. Both reference and query species need a subdirectory called chromseqs to hold the per scaffold .fa files. They can be generated in chromseqs using [faSplit](http://hgdownload.cse.ucsc.edu/admin/exe/linux.x86_64/) (refer to the end of this document for a complete explanation of Champagne's directory structure): `faSplit byname [query].fa chromseqs/`

Now use _scripts/getspeciesfiles.sh_ for each query species to generate the pickled chain files. This script additionally validates that all input files needed by Champagne are in their proper locations. Specify whether to use the parasol batch system with P or NP. `scripts/getspeciesfiles.sh P hg38 canFam3 [Champagne_Directory]`

In the _species/_ directory, put _namemap.txt_. This file should have one entry per line, where each entry is a species and its assembly name, with a colon delimiter (e.g. canFam3:dog). This file can hold all species used across distinct trees.

---

Champagne is ready to run:

**Step 1) We generate possible evidence (indels) for each gene using _phyloPipe.py_. The arguments are as follows:**

`python3 phyloPipe.py -s 0.55 -m 50 -w 30 -t CAT-DOG-PIG -r hg38 -g 1 20000 canFam3 felCat8 susScr3`

- -np will not use parasol batch system. Instead runs each job sequentially. Should only be used for demo purposes.
- -s minimum similarity required for indels to match. (Default = 0.55)
- -m minimum size of indels. (Default = 50)
- -w minimum size of the window on either side of the indel. (Default = 30)
- -t tree name, used to identify this run. The reference species and date is automatically appended
- -r reference species
- -g [x, y] look at reference genes from x to y. Set to 1 y, where (y >= number of genes) to look at all possible genes
- no flag, list of query species comes at the end, space-separated
- Note: Further evidence filtering (similarity threshold, minimum size) can be done later in the pipeline. These values here should be the absolute minimum thresholds.

**Step 2) We cluster our evidence into a single file using _phyloPipe.py_ again, this time with the -c flag. Use the full tree name.**

`python3 phyloPipe.py -c -r hg38 -t CAT-DOG-PIG-hg38-20XX-XX-XX`

**Step 3) Using _get\_branch\_scores.py_, we generate our character matrix in a nexus file, which is outputted to _paup-evidence/_. The insertion similarity threshold, deletion similarity threshold, and minimum size are appended to the full tree name to make the output file name. Additionally, we can use stricter parameters for filter evidence based on sequence similarity and indel size. We also see evidence counts for a specific topology. The arguments are as follows:**

- -n a newick-format topology, not including the reference 
- -o the outgroup or reference species
- -t full tree name
- -i the minimum similarity threshold for insertion-based evidence (Default = 0.60)
- -d the minimum similarity threshold for deletion-based evidence (Default = 0.60)
- -m the minimum size for considered indels (Default = 50)

`python3 scripts/get_branch_scores.py -n '((canFam3,felCat8),susScr3)' -o hg38 -t CAT-DOG-PIG-hg38-20XX-XX-XX -i 0.65 -d 0.60 -m 50`

**Step 4) The resulting nexus file can be used in a phylogeny inference program, such as [PAUP\*](https://paup.phylosolutions.com/). Within the PAUP\* command line interface, the following commands can be used to generate a maximum-parsimony-based topology with relevant statistics, such as retention index.**

- Load the desired nexus evidence file. `execute paup-evidence/CAT-DOG-PIG-hg38-20XX-XX-XX-0.60-0.60-50.nex`
- Set to maximum parsimony. `set criterion=parsimony`
- Run the algorithm. `hsearch`
- See the topology and statistics. `describetrees`

---
Python Prequisites:

- [numpy](https://numpy.org/)
- [Biopython](https://biopython.org/)
- [parasail](https://pypi.org/project/parasail/)

**Champagne has a config.ini file (found in _scripts/_) where both parameters and directory paths can be changed for user preference.**

Config file parameter definitions:

- minSize: The minimum size for an indel to be considered as evidence
- windowSize: The size of the window which is looked at on either side of the indel
- minSimilarity: The minimum similarity threshold for indels
- maxSize: The maximum size for an indel to be considered as evidence
- stepSize: The size of the step Champagne takes as it scans the gene for indels
- margin: Maximum basepair misalignment between the starts and ends of two indels
- genesPerJob: number of genes submitted in each cluster job
- ithreshold: The similarity threshold for insertions
- dthreshold: The similarity threshold for deletions

Path definitions and suggested Champagne directory structure (\* means user-created directory):

- phylodir -- Overarching directory, holds _phyloPipe.py_
	- inputsdir -- Holds the reference gene set and the scoring matrix
		- hoxd55.txt
		- [reference]\_genes.byID.bed
	- paupdir -- Holds the output nexus files
	- cachedir -- Used for PYTHON\_EGG\_CACHE (stays empty)
	- scripts -- Not a modifiable path, should be a subdirectory of phylodir
	- speciesdir -- Holds all species subdirectories as well as the namemap
		- [query\_species\_dir]\* -- Holds all needed files for a species
			- chromseqs\* -- Holds the per-scaffold sequence files
			- ref-[reference]\* -- Holds all 
				[reference].[query].all.chain
				[query].trimmed.chain_ids
				- pickledChains -- holds pickled chain files created by getspeciesfiles.sh
	- treedir -- Holds all tree directories, which are created during each run of Champagne
		- [tree\_name\_dir] -- contents created by phyloPipe.py and subsequent scripts
	- tmpdir -- A directory to hold temp files made during chain pickling
	- python2packages -- Holds necessary python packages
	- python3packages -- Holds necessary python packages

---
**Running Champagne Demo**

The following steps will demonstrate the workflow of Champagne by generating a matrix for the pig/cow/dog (human outgroup) trio. We have provided a number of files and directories to minimize preprocessing steps. 

Step 1) Download this repository.

`git clone https://bitbucket.org/bejerano/champagne`

Step 2) Modify _scripts/config.ini_ to make sure all directory paths are correct.

Step 3) Run _init\_for\_demo.sh_ with the path to Champagne's species directory to download the necessary species files from UCSC Goldenpath.

`./init_for_demo.sh /path/to/CHAMPAGNE/species/`

Step 4) For each species (human, dog, cat, pig), generate per-scaffold/per-chromosome fasta files using the [faSplit](http://hgdownload.cse.ucsc.edu/admin/exe/linux.x86_64/) utility. These split fasta files should go into the chromseqs/ directory.

`faSplit byname /path/to/CHAMPAGNE/species/canFam3/canFam3.fa /path/to/CHAMPAGNE/species/canFam3/chromseqs`

Step 5) For each query species, run _scripts/getspeciesfiles.sh_ to generate pickled chain files and validate that the other required species files are in their proper locations. In the arguments list, use NP instead of P if you do not use the parasol batch system.

`scripts/getspeciesfiles.sh P hg38 canFam3 /path/to/CHAMPAGNE/`

Step 6) Champagne can now be run following the steps in the main body of the README. If you are not using the parasol batch system, we suggest either modifying the code to support your own parallelization scheme or running Champagne on a very limited number of genes. If you are using this demo without parallelization, use the `-np` flag and use `-g 14443 14446` instead of `-g 1 20000`. Champagne will scan just 3 specific genes (to minimize computational time) that will provide evidence so that you can see how the rest of the steps work. 


