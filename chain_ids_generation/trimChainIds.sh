#!/bin/bash
reference=$1
query=$2

cp ${query}.chain_ids ${reference}.${query}.tmp
chainid_path=${reference}.${query}.tmp
echo "Trimming chainIDs."
col=$(head -n 1 $chainid_path | awk '{print $1}')
idx=$(awk -v tgt=$col 'BEGIN {print index(tgt,"0")-1}')
char=$(awk -v tgt=$col -v idx=$idx 'BEGIN {print substr(tgt, idx, 1)}')
if [[ $char = "G" ]]; then 
    awk '{print $1,$2}' $chainid_path | sort -k1.4n | uniq > ${query}.trimmed.chain_ids
else 
    awk '{print $3,$2}' $chainid_path | sort -k1.4n | uniq > ${query}.trimmed.chain_ids
fi
rm ${reference}.${query}.tmp
