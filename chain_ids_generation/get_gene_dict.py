
import pickle
import sys

species = sys.argv[1]

genes_chr = {} #maps gene_id -> chromosome
genes_coords = {} #maps gene_id -> coords of largest transcript for that gene

genes_transcripts = {} #maps gene_id -> list of transcript ids
genes_transcript_ids = {} #maps gene_id -> id of largest transcript
transcripts_coords = {} #maps transcript_id -> list of exon coordinates

#chroms = ['chr' + str(i) for i in range(20)]
#chroms.append('chrX')
#chroms.append('chrY')

count = 0 #total length of all exons
#populating genes_chr, genes_transcripts, transcripts_coords
for line in open(species+'_exons.txt'):
    # __exons.txt
    #gene_id    transcript_id   chromosome  exon_start  exon_end

    words = line.split('\t')
    gene_id = words[0]
    transcript_id = words[1]
    chrom = words[2]
    if 'chr' not in chrom.lower():
        chrom = 'chr'+ chrom
    exon_start = int(words[3])
    exon_end = int(words[4])
#    if chrom in chroms:
    if True:
#        count += 1
        exons = transcripts_coords.get(transcript_id, [])
        exons.append((exon_start, exon_end))
        transcripts_coords[transcript_id] = exons #transcripts_coords[transcript_id] = [(exon1_start, exon1_end),(exon2_start, exon2_end),...]
        genes_chr[gene_id] = chrom
        transcripts = genes_transcripts.get(gene_id, [])
        if transcript_id not in transcripts:
            transcripts.append(transcript_id)
            genes_transcripts[gene_id] = transcripts
#populates genes_coords, genes_transcript_ids
for gene_id in genes_chr.keys():
    max_bases = 0
    for transcript_id in genes_transcripts[gene_id]:
        bases = sum([c[1] - c[0] for c in transcripts_coords[transcript_id]])
        if bases > max_bases:
            max_bases = bases
            genes_coords[gene_id] = transcripts_coords[transcript_id]
            genes_transcript_ids[gene_id] = transcript_id
    count += len(genes_coords[gene_id])

pickle.dump(genes_coords, open(species+'_genes_coords.p', 'w'))
pickle.dump(genes_chr, open(species+'_genes_chr.p', 'w'))
#print genes_transcript_ids
print count #total length of all exons
print len(genes_chr.keys()) #number of genes

#k is gene id, v is transcript id of largest transcript corresponding to that gene
for k, v in genes_transcript_ids.items():
    print k, v



