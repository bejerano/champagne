#!/bin/bash
reference=$1
query=$2
join -1 1 -2 1 <(grep "\[" ${query}.out | cut -d' ' -f1,2  | sort -k1,1) <(sort -k1,1 ${reference}_canonical_ids.txt) > ${query}.chain_ids
