#!/usr/bin/python3.4
''''
###################
#### phyloPipe ####
###################

Wrapper script for phylogenomic pipeline. Documentation in README.md. 
'''

import sys
import os 
import csv
import datetime
import argparse
import configparser as cp
import subprocess
import numpy as np
from collections import defaultdict
from Bio import Phylo
from pprint import pprint



# ---- ARG PARSER -----------------------------------------------------

parser = argparse.ArgumentParser()

parser.add_argument("-np", "--no-parasol",
                    action='store_true',
                    help="Will not use the parasol system for parallelization")
parser.add_argument("-c", "--cluster",
                    action='store_true',
                    help="Cluster")
parser.add_argument("-o", "--output",
                    action='store_true',
                    help="Print output of clustering, rather than save to file.")
parser.add_argument("-d", "--delsOnly",
                    action='store_true',
                    help="Only consider deletions as evidence.")
parser.add_argument("-i", "--insOnly",
                    action='store_true',
                    help="Only consider insertions as evidence.")
parser.add_argument("-s", "--minSimilarity",
                    type=float,
                    help="Required similarity threshold.")
parser.add_argument("-m", "--minSize",
                    type=int,
                    help="Minimum insertion/deletion size.")
parser.add_argument("-f", "--filter",
                    type=int,
                    help="Filter indels below this size (only applicable to clustering step).")
parser.add_argument("-w", "--windowSize",
                    type=int,
                    help="Size of window comparison (on either side).")
parser.add_argument("-e", "--exclude_taxa",
                    nargs='*',
                    help="Exclude one or more taxa from clustering.")
parser.add_argument("-g", "--genes",
                    type=int,
                    nargs=2,
                    help="Range of genes to process.")
parser.add_argument("-r", "--reference",
					required=True)
parser.add_argument("-t", "--treeName",
					required=True,
					help="Tree name.")
parser.add_argument('species', nargs='*', help="Species in tree.")

args = parser.parse_args()



# --------------------------------------------------------------------
# ----- CONFIG PARSER ------------------------------------------------
config = cp.ConfigParser(interpolation=cp.BasicInterpolation())
config.read("scripts/config.ini")

GENES_PER_JOB = int(config["Parameters"]["genesPerJob"])
PHYLO_DIR = config["Paths"]["phylodir"]
NWAY_SPLIT = False
#Maps the genome code to the species eg {'hg38' : 'human'}
nameMap = {line.split(':')[0].strip() : line.split(':')[1].strip() for line in open(PHYLO_DIR+'species/namemap.txt', 'r').readlines()}
#Maps the species to the genome code eg {'human' : 'hg38'}
reverseNameMap = {line.split(':')[1].strip() : line.split(':')[0].strip() for line in open(PHYLO_DIR+'species/namemap.txt', 'r').readlines()}
# --------------------------------------------------------------------

# Takes speciesStatus of format mm10+:0.75 and returns the species (mm10)
def speciesFromSpeciesStatus(speciesStatus):
	return speciesStatus.split(':')[0][:-1]

# Takes speciesStatus of format mm10+:0.75 and returns the sim (0.75)
def simFromSpeciesStatus(speciesStatus):
	sim = speciesStatus.split(':')[1]
	return float(sim) if sim != '~' else sim

# Takes speciesStatus of format mm10+:0.75 and returns the status (+)
def statusFromSpeciesStatus(speciesStatus):
	return speciesStatus.split(':')[0][-1]

def lengthen_node_name(node):
	changed_node = []
	curr_str = ''
	pos = 0
	while pos<len(node):
		if node[pos].isalnum():
			if curr_str != '' and not curr_str[0].isalnum(): 
				changed_node += [curr_str]
				curr_str = ''
		else:
			if curr_str in nameMap: 
				changed_node += ['-'.join(nameMap[curr_str].split())]
				curr_str = ''
		curr_str += node[pos]
		pos += 1
	if curr_str in nameMap: 
		changed_node += [nameMap[curr_str]]
	else:
		changed_node += [curr_str]
	return ''.join(changed_node)

def print_tree(nodes, treeDir):
	unclustered = [node for node in nodes if not ')' in node]
	clustered = [node for node in nodes if ')' in node]
	longClustered = []
	
	for node in clustered:
		longClustered.append(lengthen_node_name(node))

	newick_fmt = ", ".join(clustered)
	long_newick_fmt = ', '.join(longClustered)
	with open(treeDir+'/newick_tree.txt', 'w') as f1:
		f1.write(newick_fmt + '\n')
	with open(treeDir+'/newick_tree_long.txt', 'w') as f2:
		f2.write(long_newick_fmt + '\n')
	tree = Phylo.read(treeDir+'/newick_tree_long.txt', "newick")
	Phylo.draw_ascii(tree)

	return tree

def getS_modified(pair, evidence, species):

	# Cases maps from cross-node pairing to [count, [actual cases]]
	cases = defaultdict(list)
	nodeA = [s.strip().replace('(', '').replace(')', '') for s in pair[0].split(',')]
	nodeB = [s.strip().replace('(', '').replace(')', '') for s in pair[1].split(',')]
	excluded = [s for s in species if s not in nodeA+nodeB]

	pairings = [(a, b) for a in nodeA for b in nodeB]

	for indel, status in evidence.items():
		type_ = indel.split()[0]
		for pairing in pairings:
			a, b = pairing
			for x in excluded:
				add = False
				if type_ == 'insertion':
					if status[a][0] == '+' and status[b][0] == '+' and status[x][0] == '-': add = True
				elif type_ == 'deletion':
					if status[a][0] == '-' and status[b][0] == '-' and status[x][0] == '+': add = True
				if add:
					if indel in cases[pairing]: continue
					if len(cases[pairing])==0:
						cases[pairing].append(1)
						cases[pairing].append([indel])
					else:
						cases[pairing][0] += 1
						cases[pairing][1].append(indel)

	S = 1 + sum([val[0]/float(len(excluded)) for key, val in cases.items()])/float(len(pairings))

	return S, cases

def getC_modified(pair, evidence, species):
	cases = defaultdict(list)
	nodeA = [s.strip().replace('(', '').replace(')', '') for s in pair[0].split(',')]
	nodeB = [s.strip().replace('(', '').replace(')', '') for s in pair[1].split(',')]
	excluded = [s for s in species if s not in nodeA+nodeB]

	pairings = [(a, b) for a in nodeA for b in nodeB]

	for indel, status in evidence.items():
		type_ = indel.split()[0]
		for pairing in pairings:
			a, b = pairing
			for x in excluded:
				add = False
				if type_ == 'insertion':
					if status[a][0] == '-' and status[b][0] == '+' and status[x][0] == '+': add = True
					if status[a][0] == '+' and status[b][0] == '-' and status[x][0] == '+': add = True
				elif type_ == 'deletion':
					if status[a][0] == '-' and status[b][0] == '+' and status[x][0] == '-': add = True
					if status[a][0] == '+' and status[b][0] == '-' and status[x][0] == '-': add = True
				if add:
					if indel in cases[pairing]: continue
					if len(cases[pairing])==0:
						cases[pairing].append(1)
						cases[pairing].append([indel])
					else:
						cases[pairing][0] += 1
						cases[pairing][1].append(indel)

	C = 1 + sum([val[0]/float(len(excluded)) for key, val in cases.items()])/float(len(pairings))

	return C, cases


def load_evidence(treeDir):

	command = input("Should we join all evidence? Only join if evidence.txt is not up to date. (y/n): ")
	if command[0] == 'y':
		remove_files = input("Should we remove individual evidence files? (yes/n): ")
		for i in range(10):
			subprocess.call('cat {}/evidence/*{}.txt > {}/evidence{}.txt'.format(treeDir, i, treeDir, i), shell=True)
			if remove_files == 'yes':
				subprocess.call('rm {}/evidence/*{}.txt'.format(treeDir, i), shell=True)			
			print("Joined evidence for genes ending in {}.".format(i))

		subprocess.call('cat {}/evidence?.txt > {}/evidence.txt'.format(treeDir, treeDir), shell=True)
		subprocess.call('rm {}/evidence?.txt'.format(treeDir), shell=True)				

	evidence = {}
	for line in open(treeDir + '/evidence.txt', 'r').readlines():
		line = line.split()
		if (args.delsOnly and line[0]=='insertion') or (args.insOnly and line[0]=='deletion'): continue
		evidence[' '.join(line)] = {speciesFromSpeciesStatus(s):(statusFromSpeciesStatus(s), simFromSpeciesStatus(s)) for s in line[5:] if not s.startswith(args.reference)}
	return evidence

# Performs agglomerative clustering on a tree for which all genes have been processed.
def cluster(treeName, evidence, ithreshold, dthreshold):

	# Initialize paths and directories
	treeDir = PHYLO_DIR+'trees/'+treeName
	thresholdTreesDir = '{}/thresholdTrees'.format(treeDir)
	treeFile = '{}/IT={}_DT={}.tree'.format(thresholdTreesDir, ithreshold, dthreshold)
	print("Tree will be stored in {}.".format(treeFile))
	if not os.path.isdir(thresholdTreesDir):
		os.makedirs(thresholdTreesDir)

	# Remove any specified taxa from the species that will be included in the clustering
	if args.exclude_taxa is not None:
		print("\nWe'll be exluding the following species: {}.".format(', '.join(args.exclude_taxa)))	
	species = [s.strip() for s in open(treeDir+'/summary.txt', 'r').readlines()[1:]]
	if args.exclude_taxa is not None:	
		for t in args.exclude_taxa:
			species.remove(t)

	# Initialize data structures
	clusterMatrix = defaultdict(dict)
	cases = defaultdict(dict)
	counts = defaultdict(dict)
	allsupporting, allconflicting = set([]), set([])
	for s in species: clusterMatrix[s] = {}

	print("\n\n*-------------------------------------*")

	# Cluster until convergence
	closest = [None, None]
	joinedNode = None

	print("Peforming agglomerative clustering. Results will be written to {}.\n".format(treeFile))
	print("Clustering will be performed on the basis of {} unique evidence cases.".format(len(list(set(evidence.keys())))))
	if not args.output:
		out = open(treeFile, 'w')
		orig_stdout = sys.stdout
		sys.stdout = out

	################################################################
	# - We cluster nodes by max(S/C)
	# - We also track the minimum of these scores across the tree, to
	# allow comparison with other IT/DT thresholds
	################################################################
	score_of_worst_branch, worst_score_iter = float('inf'), None
	worst_branch_S, worst_branch_C = None, None
	iteration = 0
	while len(clusterMatrix.keys()) > 1:
		iteration+=1
		print("\n--- Iteration {}: ---".format(iteration))

		# 1. Calculate S/C for all uncalculated pairs
		if joinedNode is None:
			for i, s1 in enumerate(clusterMatrix.keys()):
				for j, s2 in enumerate(clusterMatrix.keys()):
					if j <= i: continue
					pair = [s1, s2]
					S, S_cases = getS_modified(pair, evidence, species)
					C, C_cases = getC_modified(pair, evidence, species)
					pairName = ','.join(sorted(pair))
					cases[pairName]['S'], cases[pairName]['C'] = S_cases, C_cases
					counts[pairName]['S'], counts[pairName]['C'] = S, C
					clusterMatrix[s1][s2], clusterMatrix[s2][s1] = round(S/float(C), 2), round(S/float(C),2)


		# Print keys of score-matrix (i.e. species names)
		current_keys = list(clusterMatrix.keys())
		writer = csv.DictWriter(sys.stdout, restval="NA", fieldnames=current_keys)
		writer.writeheader()
		
		# 2. Find closest pair
		max_score, closestS, closestC = 0, 0, 0
		backup_closest, backup_closestS, backup_closestC = [], 0, 0 # in case of only S/C=1
		for nodeA in current_keys:
			writer.writerow(clusterMatrix[nodeA])
			for nodeB in clusterMatrix.keys():
				if nodeA == nodeB: continue
				pairName = ','.join(sorted([nodeA, nodeB]))
				S, C = counts[pairName]['S'], counts[pairName]['C']
				branch_score = clusterMatrix[nodeA][nodeB]
				if branch_score > max_score and S>1:
					max_score = branch_score
					closest, closestS, closestC = [nodeA, nodeB], S, C
				elif branch_score > max_score:
					backup_closest, backup_closestS, backup_closestC = [nodeA, nodeB], S, C
		if max_score == 0: 
			closest, closestS, closestC = backup_closest, backup_closestS, backup_closestC

		nodeA, nodeB = closest[0], closest[1]
		pairName = ','.join(sorted([nodeA, nodeB]))
		SC_closest = clusterMatrix[nodeA][nodeB]

		# Only update if > 0 to avoid score on last iteration (which will always be 0)
		if max_score < score_of_worst_branch and max_score > 0: 
			score_of_worst_branch, worst_score_iter = max_score, iteration
			worst_branch_S, worst_branch_C = closestS, closestC
		print("\nClosest pair is {} and {}, with S={}, C={}, S/C={}.".format(lengthen_node_name(nodeA), lengthen_node_name(nodeB), round(closestS, 2), round(closestC, 2), SC_closest))
		
		# Once we've determined the nodes that will be clustered,
		# we write to the cases/ subdirectory with the supporting and conflicting
		# cases for that decision. We also print the number of unique cases that 
		# factored into the decision. 

		uniqueSupporting, uniqueConflicting = [], []
		if args.delsOnly: cases_prefix = 'deletions_'
		elif args.insOnly: cases_prefix = 'insertions_'
		else: cases_prefix = ''
		if not os.path.isdir("{}/{}cases".format(treeDir, cases_prefix)): os.makedirs("{}/{}cases".format(treeDir, cases_prefix))
		with open("{}/{}cases/iter{}_S_cases".format(treeDir, cases_prefix, iteration), 'w') as f:
			for pairing, pairingCases in cases[pairName]['S'].items():
				f.write('Cases for pairing {}:\n'.format(pairing))
				f.write('\n'.join(pairingCases[1]) + '\n\n')
				uniqueSupporting += pairingCases[1]
				for case in pairingCases[1]:
					allsupporting.add(case)

		with open("{}/{}cases/iter{}_C_cases".format(treeDir, cases_prefix, iteration), 'w') as f:
			for pairing, pairingCases in cases[pairName]['C'].items():
				f.write('Cases against pairing {}:\n'.format(pairing))
				f.write('\n'.join(pairingCases[1]) + '\n\n')
				uniqueConflicting += pairingCases[1]
				for case in pairingCases[1]:
					allconflicting.add(case)
		print("This S/C ratio comprised {} and {} unique supporting and conflicting cases respectively.\n".format(len(list(set(uniqueSupporting))), len(list(set(uniqueConflicting)))))
		if iteration == 4:
			print('\n'.join(sorted(list(set(uniqueSupporting)), key = lambda x: x[x.index('chr'):])))

		# 3. Cluster closest pair
		clusterNodes = closest
		for nodeC in current_keys:
			if nodeC==nodeA or nodeC==nodeB: continue
			pairNameA, pairNameB = ','.join(sorted([nodeA, nodeC])), ','.join(sorted([nodeB, nodeC])) 
			nConflictingA, nConflictingB = counts[pairNameA]['C'], counts[pairNameB]['C']
			nSupportingA, nSupportingB = counts[pairNameA]['S'], counts[pairNameB]['S']
			SC_A, SC_B = round(nSupportingA/float(nConflictingA), 2), round(nSupportingB/float(nConflictingB), 2)
			print("For {}, A: [supporting={}, conflicting={}, S/C={}], B: [supporting={}, conflicting={}, S/C={}]".format(lengthen_node_name(nodeC), round(nSupportingA, 2), round(nConflictingA, 2), SC_A, round(nSupportingB, 2), round(nConflictingB, 2), SC_B))
			if NWAY_SPLIT is True and (SC_closest/SC_A <= 1 or SC_closest/SC_B <= 1):
				clusterNodes.append(nodeC)
				print("...But we can't rule out {}, so adding to newly clustered node.".format(lengthen_node_name(nodeC)))

		joinedNode = '({})'.format(','.join(clusterNodes))
		print("\nNewly clustered node: {}".format(lengthen_node_name(joinedNode)))
		for newlyClustered in clusterNodes: clusterMatrix.pop(newlyClustered)
		for node in list(clusterMatrix.keys()): 
			for newlyClustered in clusterNodes: clusterMatrix[node].pop(newlyClustered)

		if len(list(clusterMatrix.keys())) == 0:
			print("Tree complete!")
			tree = print_tree([joinedNode], treeDir)
			print(joinedNode)
			break

		for node in list(clusterMatrix.keys()):
			pair = [joinedNode, node]
			S, S_cases = getS_modified(pair, evidence, species)
			C, C_cases = getC_modified(pair, evidence, species) 
			pairName = ','.join(sorted(pair))
			cases[pairName]['S'], cases[pairName]['C'] = S_cases, C_cases
			counts[pairName]['S'], counts[pairName]['C'] = S, C
			clusterMatrix[joinedNode][node], clusterMatrix[node][joinedNode] = round(S/float(C),2), round(S/float(C),2)

		print_tree(list(clusterMatrix.keys()), treeDir)

	totalNsupporting, totalNconflicting = len(allsupporting), len(allconflicting)
	print("All supporting", "\n".join(allsupporting))
	print("All conflicting", "\n".join(allconflicting))
	print("\nThis tree contained a total of {} supporting cases and {} conflicting cases.".format(totalNsupporting, totalNconflicting))
	
	# Calculate score
	if len(allconflicting)==0: 
		score = len(allsupporting)
	else:
		score = round(len(allsupporting)/float(len(allconflicting)), 2)
	print("Total S/C (i.e. score of tree) --> {}.".format(score))
	print("The score of the weakest branch in the tree was {}, on iteration {}, with S={}, C={}.".format(score_of_worst_branch, worst_score_iter, worst_branch_S, worst_branch_C))

	if not args.output:
		sys.stdout = orig_stdout
		out.close()
	print("Done.")

	# If there's no evidence, we don't want to use this threshold set
	if score_of_worst_branch == float('inf'):
		score_of_worst_branch = -float('inf')

	return totalNsupporting, totalNconflicting, score_of_worst_branch, tree

def filter_evidence(evidence, ithreshold, dthreshold):
	filtered_evidence = {}

	for case in evidence.keys():
		status = case.split()

		if args.filter:
			if int(status[1]) < args.filter: continue

		if status[0] == 'insertion':
			atLeastOneDeletion, numInsertions = False, 0
			for species in status[5:]:
				if speciesFromSpeciesStatus(species)==args.reference: continue
				if statusFromSpeciesStatus(species)=='-' and simFromSpeciesStatus(species)>=ithreshold: atLeastOneDeletion = True
				if statusFromSpeciesStatus(species)=='+' and simFromSpeciesStatus(species)>=ithreshold: numInsertions+=1
			if atLeastOneDeletion and numInsertions>=2: filtered_evidence[case] = evidence[case]

		elif status[0] == 'deletion':
			atLeastOneInsertion, numDeletions = False, 0
			for species in status[5:]:
				if speciesFromSpeciesStatus(species)==args.reference: continue
				if statusFromSpeciesStatus(species)=='-' and simFromSpeciesStatus(species)>= dthreshold: numDeletions+=1
				if statusFromSpeciesStatus(species)=='+' and simFromSpeciesStatus(species)>= dthreshold: atLeastOneInsertion = True
			if atLeastOneInsertion and numDeletions>=2: filtered_evidence[case] = evidence[case]

	return filtered_evidence

def testThresholds(treeName):
	
	thresholds = [0.6, 0.625, 0.65, 0.675, 0.7]

	if args.filter:
		print("We'll be filtering evidence smaller than {}bp.".format(args.filter))
	else:
		print("No filter was specified, so we will include evidence of all available sizes.")
	print("We'll test the following thresholds: {}. This will require {} tests.".format(thresholds,len(thresholds)**2))
	greenlight = input("Continue? (y/n)")
	if greenlight!='y': exit()
	treeDir = PHYLO_DIR+'trees/'+treeName
	if not os.path.isdir('{}/topTreesEvidence/'.format(treeDir)):
		os.makedirs('{}/topTreesEvidence/'.format(treeDir))

	# 1. Load ALL evidence
	results = {}
	evidence = load_evidence(treeDir)
	print("There are {} total usable evidence cases.".format(len(list(evidence.keys()))))

	# 2. Calculate statistics for all threshold pairs
	threshold_scores = {}
	for ithreshold in thresholds:
		for dthreshold in thresholds:

			# enforce IT >= DT
			if ithreshold < dthreshold: continue
			print("####### Testing ithreshold={}, dthreshold={} #######".format(ithreshold, dthreshold))
			evidence_at_threshold = filter_evidence(evidence, ithreshold, dthreshold)
			print("There are {} evidence cases usable at this threshold.".format(len(list(evidence_at_threshold.keys()))))
			totalNsupporting, totalNconflicting, score_on_worst_branch, tree = cluster(treeName, evidence_at_threshold, ithreshold, dthreshold)
			results[('IT={}'.format(ithreshold), 'DT={}'.format(dthreshold))] = (totalNsupporting, totalNconflicting, score_on_worst_branch, tree)
			print("########################################################\n")

	# 3. Print and log results
	print("Results of threshold testing:")
	with open(treeDir + '/topTrees.txt', 'w') as out:
		for i, threshold in enumerate(sorted(list(results.keys()), key=lambda x: float(results[x][2]), reverse=True), 1):
			if i>5: break

			# a) Print tree
			print("Tree {}: Threshold = {}, with worst-branch-score {}, {} supporting cases, and {} conflicting cases".format(i, threshold, results[threshold][2], results[threshold][0], results[threshold][1]))
			out.write("Tree {}: Threshold = {}, with worst-branch-score {}, {} supporting cases, and {} conflicting cases".format(i, threshold, results[threshold][2], results[threshold][0], results[threshold][1])+'\n')
			Phylo.draw_ascii(results[threshold][3])

			# b) Log tree to file
			orig_stdout = sys.stdout
			sys.stdout = out
			Phylo.draw_ascii(results[threshold][3])
			sys.stdout = orig_stdout

			# c) Store evidence for top three trees
			if i<=3:
				ithreshold, dthreshold = float(threshold[0].split('=')[1]), float(threshold[1].split('=')[1])
				evidence_at_threshold = filter_evidence(evidence, ithreshold, dthreshold)
				with open('{}/topTreesEvidence/IT={}-DT={}.txt'.format(treeDir, ithreshold, dthreshold), 'w') as ev:
					for case in list(evidence_at_threshold.keys()):
						ev.write(case + '\n')

	return

def create_job(gene, geneLines, jobdir, treeName, minSize, windowSize, simThreshold):

	if not os.path.isdir('trees/{}/genes/'.format(treeName)):
		os.makedirs('trees/{}/genes/'.format(treeName))
	if not os.path.isdir('trees/{}/MSA/'.format(treeName)):
		os.makedirs('trees/{}/MSA/'.format(treeName))	
		
	with open('trees/{}/genes/{}.txt'.format(treeName, gene), 'w') as f:
		for line in geneLines:
			f.write('\t'.join(line) + '\n')

	if not os.path.isdir(jobdir + '/scripts'):
		os.makedirs(jobdir + '/scripts')

	with open(jobdir + '/job_{}'.format(gene), 'w') as jobfile:
		scriptname = jobdir + '/scripts/job_'+str(gene)+'.sh'
		with open(jobdir + '/scripts/job_'+str(gene)+'.sh', 'w') as script:
			script.write('#!/bin/sh' + '\n')
			script.write('export PYTHON_EGG_CACHE={}'.format(config["Paths"]["cachedir"]) + '\n')
			script.write('export PYTHONPATH=$PYTHONPATH:{}'.format(config["Paths"]["python2packages"]) + '\n')
			script.write('export PYTHONPATH=$PYTHONPATH:{}'.format(config["Paths"]["python3packages"]) + '\n')
			script.write('python {}scripts/searchGene.py'.format(PHYLO_DIR) + ' {} {} {} {} {} {}'.format(treeName, gene, simThreshold, minSize, windowSize, args.reference) + '\n')
		jobfile.write(scriptname + '\n')
	return scriptname

# Traverses labelled genes, and creates one job per 
# gene. 
def send_jobs(genesPerJob, treeName, minSize, windowSize, simThreshold):


	if args.no_parasol:
		print("-np flag detected. Not using the Parasol Parallel Batch System.\nFor info on Parasol: https://genecats.gi.ucsc.edu/eng/parasol.html\nRunning jobfiles sequentially (may take a long time)...")
	jobdir = PHYLO_DIR + 'trees/{}/jobs'.format(treeName)
	sitesfile = 'trees/{}/labelled_genes.txt'.format(treeName)
	with open(sitesfile, 'r') as f:
		for geneNum, line in enumerate(f.readlines()):
			line=line.split()
			if geneNum == args.genes[1]: 
				print("Reached end of range.")
				break
			if geneNum >= args.genes[0]: 
				print("Creating job for gene {}: {}.".format(geneNum, line[0]))
				scriptfile = create_job(line[0], [line], jobdir, treeName, minSize, windowSize, simThreshold)
				if args.no_parasol:
					print("Running scriptfile {}".format(scriptfile))
					subprocess.call("chmod +x {}".format(scriptfile), shell=True)
					subprocess.call(scriptfile, shell=True)

	
	if not args.no_parasol:
		with open(jobdir + '/command', 'w') as para_command:
			para_command.write('#!/bin/sh' + '\n')
			para_command.write('cd {}\n'.format(jobdir))
			para_command.write("chmod +x scripts/*" + '\n')
			para_command.write("para create job -ram=18g -cpu=1 -maxJob=50" + '\n')
			para_command.write("para push" + '\n')

		for i in range(10):
			subprocess.call("echo '' | cat > {}/job_{}".format(jobdir, i), shell=True)
			subprocess.call("cat {}/job_*{} > {}/job{} ; rm {}/job_*{}".format(jobdir, i, jobdir, i, jobdir, i), shell=True)
		subprocess.call("cat {}/job? > {}/job".format(jobdir, jobdir), shell=True)

		subprocess.call('chmod +x {}/command'.format(jobdir), shell=True)
		subprocess.call("{}/command".format(jobdir), shell=True)
		print("Submitting jobs for all genes.")


# Labels the sitesfile with chainIDs for 
# all N species. 
def label_with_chainIDs(species, treeName, sitesfile):

	subprocess.call("scp {} trees/{}/tmpA".format(sitesfile, treeName), shell=True)
	for c, s in enumerate(species):
		subprocess.call("cat species/{}/ref-{}/{}.trimmed.chain_ids | uniq > species/{}/ref-{}/{}.trimmed.chain_ids.tmp ; mv species/{}/ref-{}/{}.trimmed.chain_ids.tmp species/{}/ref-{}/{}.trimmed.chain_ids".format(s, args.reference, s,s, args.reference, s,s,args.reference, s,s, args.reference, s), shell=True)
		print("Labelling with {} chainIDs.".format(s))
		command1 = "join -1 1 -2 1 -a1 -e 'NULL' -o 0 1.2 1.3 1.4 {} 2.2 \
			    trees/{}/tmpA species/{}/ref-{}/{}.trimmed.chain_ids > trees/{}/tmpB".format(' '.join(['1.'+str(x) for x in range(5,c+5)]), treeName, s, args.reference, s, treeName)
		command2 = "mv trees/{}/tmpB trees/{}/tmpA".format(treeName, treeName)
		subprocess.call(command1, shell=True)
		subprocess.call(command2, shell=True)
	subprocess.call("cat trees/{}/tmpA | uniq > trees/{}/labelled_genes.txt".format(treeName, treeName), shell=True)

def main():

	if args.cluster:
		ready = input("Are you sure that all the genes in {} have been processed? (y/n): ".format(args.treeName))
		if ready[0].lower() == 'y':
			# split = input("Would you like to allow for N-way splits? (y/n): ")
			# if split[0].lower() == 'y': NWAY_SPLIT = True
			testThresholds(args.treeName)
	else: 
		if not args.genes: 
			print("You must specify the range of genes to process.") 

		treeName = args.treeName + '-{}-{}'.format(args.reference, datetime.date.today())

		if not args.minSize:
			minSize = int(config["Parameters"]["minSize"])
			print("Using default minimum indel size of {} from config.ini.".format(minSize))
		else:
			print("Using specified minimum indel size of {}.".format(args.minSize))
			minSize = int(args.minSize)

		if not args.windowSize:
			windowSize = int(config["Parameters"]["windowSize"])
			print("Using default window size of {} from config.ini.".format(windowSize))
		else:
			print("Using specified window size of {}.".format(args.windowSize))
			windowSize = int(args.windowSize)

		if not args.minSimilarity:
			simThreshold = float(config["Parameters"]["minSimilarity"])
			print("Using default minimum similarity of {} from config.ini.".format(simThreshold))
		else:
			print("Using specified minimum similarity of {}.".format(args.minSimilarity))
			simThreshold = float(args.minSimilarity)

		greenlight = input("Continue? (y/n)")
		if greenlight!='y': exit()

		species = args.species
		sitesfile = PHYLO_DIR + 'inputs/{}_genes.byID.bed'.format(args.reference)
		if not os.path.exists(sitesfile): 
			print("The expected file {} doesn't exist.".format(sitesfile))
		print("Running pipeline with minSim={}, minSize={}, windowSize={}.".format(simThreshold, minSize, windowSize))

		# Initialize directory
		if not os.path.isdir('trees/{}/log'.format(treeName)): 
			os.makedirs('trees/{}/log'.format(treeName))
		if not os.path.isdir('trees/{}/evidence'.format(treeName)): 
			os.makedirs('trees/{}/evidence'.format(treeName))
		if not os.path.isdir('trees/{}/negatives'.format(treeName)): 
			os.makedirs('trees/{}/negatives'.format(treeName))
		if not os.path.isdir('trees/{}/failed-no-sequence'.format(treeName)): 
			os.makedirs('trees/{}/failed-no-sequence'.format(treeName))
		if not os.path.isdir('trees/{}/jobs'.format(treeName)):
			os.makedirs('trees/{}/jobs'.format(treeName))
		with open('trees/{}/summary.txt'.format(treeName), 'w') as f:
			f.write("This tree was built with with minSim={}, minSize={}, windowSize={}, and contains the \
following species: \n".format(simThreshold, minSize, windowSize))
			f.write('\n'.join(species) + '\n')

		# Produce joined input file
		print("Labelling genes with species' chainIDs.")
		label_with_chainIDs(species, treeName, sitesfile)

		print("Creating jobs.")
		send_jobs(GENES_PER_JOB, treeName, minSize, windowSize, simThreshold)
	return

if __name__ == '__main__':
	main()