#!/usr/bin/python3.4

'''
Given a tree name, this script picks an insertion
at random and prints the corresponding sequences in a 
format that can be given to MUSCLE as input.

'''

import sys
import os
import random
import pickle
import argparse
import configparser as cp

parser = argparse.ArgumentParser()

parser.add_argument("-t", "--tree",
                    help="Tree name")
parser.add_argument("-e", "--evidence",
					nargs='*',
                    help="Evidence")
parser.add_argument("-s", "--min_similarity",
                    type=float,
                    help="Minimum similarity allowed on an insertion.")
parser.add_argument("-m", "--min_size",
                    type=int,
                    help="Minimum size allowed on an insertion.")
parser.add_argument("-r", "--require",
                    nargs='*',
                    help="Given the command 'speTri2+ mm10-', only selects evidence with those parameters. ")
parser.add_argument("-p", "--pickle_all_cases",
					action='store_true')
parser.add_argument("-i", "--insertions",
					action='store_true')
parser.add_argument("-d", "--deletions",
					action='store_true')
args = parser.parse_args()

config = cp.ConfigParser(interpolation=cp.BasicInterpolation())
dir_path = os.path.dirname(os.path.realpath(__file__))
config.read("{}/config.ini".format(dir_path))

# scripts/get_MSA.py -t squirrel-MSA-final-hg38-2019-07-24 -m 50 -s 0.6
# scripts/get_MSA.py -t pig-cow-dog-dynamic-hg38-2019-04-02 -m 50 -s 0.6
# scripts/get_MSA.py -t PCD-MSA-final-hg38-2019-07-29 -m 50 -s 0.6

def pickle_all_cases(msa_dir):

	indel_to_gene = {}  
	for i, gene in enumerate(os.listdir(msa_dir)):
		if gene == 'indel_to_gene.pkl': continue
		if i%100==0: print("Done {} genes.".format(i))
		MSAs = pickle.load(open(msa_dir+gene, 'rb'))
		for indel in MSAs.keys():
			indel_to_gene[indel] = gene
	pickle.dump(indel_to_gene, open(msa_dir+'indel_to_gene.pkl', 'wb'))
	return

def find_case(msa_dir):
	# print("Finding a case where {}".format(args.require))
	if args.require is None: args.require = []

	indel_to_gene = pickle.load(open(msa_dir+'indel_to_gene.pkl', 'rb'))

	possible_indels = []
	for indel in list(indel_to_gene.keys()):

		if args.deletions and indel.startswith('ins'): continue
		if args.insertions and indel.startswith('del'): continue

		# if indel.startswith('deletion 84 chr2 196771638 196771722'):
		ev = " ".join(args.evidence)
		if indel.startswith(ev):
			statuses = [stat.split(':')[0] for stat in indel.split()[5:] \
						if ':' not in stat \
						or ('~' not in stat and float(stat.split(':')[1])>=args.min_similarity)]
			possible_indels.append((indel, statuses))
			break
		else:
			continue

		if args.min_size and int(indel.split()[1])<args.min_size: continue

		statuses = [stat.split(':')[0] for stat in indel.split()[5:] \
		if ':' not in stat \
		or ('~' not in stat and float(stat.split(':')[1])>=args.min_similarity)]

		if indel.split()[0]=='deletion':
			nWithIndel = len([s for s in statuses if s[-1:]=='-'])
			nWithoutIndel = len([s for s in statuses if s[-1:]=='+'])
		else:
			nWithIndel = len([s for s in statuses if s[-1:]=='+'])
			nWithoutIndel = len([s for s in statuses if s[-1:]=='-'])
		# if args.min_similarity:
		# 	statuses = [stat[0] for stat in statuses if float(stat[1])>=args.min_similarity]

		good = True
		for status in args.require:
			if status not in statuses:
				good = False
		if good and len(statuses)>2 and nWithIndel>1 and nWithoutIndel>1: 
			possible_indels.append((indel, statuses))

	if len(possible_indels)==0: 
		print("Couldn't find any such indels.")
		return
	print("There were {} indels that fit the criteria. Picking a random one.".format(len(possible_indels)))


	indel, statuses = random.choice(possible_indels)
	statuses = [s[:-1] for s in statuses]
	print("Chose {}.".format(indel))

	print(msa_dir+indel_to_gene[indel])
	seqs = pickle.load(open(msa_dir+indel_to_gene[indel], 'rb'))[indel]

	prewindow = {'section':'prewindow'}
	indelseq = {'section':'indel'}
	postwindow = {'section':'postwindow'}

	print("-----  complete  ------")
	for species in statuses:
		print('> {}'.format(species))
		seq = seqs[species]
		prewindow[species] = seq[:30]
		if len(seq)>100:
			indelseq[species] = seq[30:-30]
		postwindow[species] = seq[-30:]
		print(seqs[species]+'\n')
	print("-----------------------\n")

	for subseq in [prewindow, indelseq, postwindow]:
		print("-----  {}  ------".format(subseq['section']))
		for species in subseq.keys():
			if species=='section':continue
			print('> ' + species)
			print(subseq[species]+'\n')
	
		print("-----------------\n")
	return

def main():

	# base = '/cluster/u/jahemker/phylo2/'
	base = config["Paths"]["phylodir"]
	tree = args.tree
	msa_dir = base+'trees/'+tree+'/MSA/'

	print("This script assumes a window size of 50bp on either side.")

	if not os.path.exists(msa_dir):
		print("There doesn't seem to exist an MSA directory for that tree.")
		return

	if args.pickle_all_cases:
		pickle_all_cases(msa_dir)
		return

	if args.require != '' and not os.path.exists(msa_dir+'indel_to_gene.pkl'):
		print("You have to run this script with the -p flag before you can filter specific indels.")

	if args.require != '':
		find_case(msa_dir)
		return

	# load available files
	available_genes = os.listdir(msa_dir)

	print('Finding an insertion with the desired properties.')
	while True:

		gene = random.choice(available_genes)
		print("Selecting MSA from {}".format(gene[:-3]))

		# pick random insertion
		MSAs = pickle.load(open(msa_dir+gene, 'rb'))
		if len(list(MSAs.keys())) == 0: 
			# print("There are no insertions logged from this gene.")
			return

		# print("There are {} insertions to choose from.".format(len(list(MSAs.keys()))))
		insertion = random.choice(list(MSAs.keys()))
		split_ins = insertion.split()

		# check properties of insertion
		# -- size
		if args.min_size and int(split_ins[1]) < args.min_size: 
			continue

		# -- similarity filters out species so that it looks better
		if args.min_similarity:	
			print("Checking min_similarity...")
			
			statuses = split_ins[5:]
			skip = False
			for s in statuses: 
				if ':' not in s or s[-1]=='~': continue
				sim = float(s.split(':')[1])
				if sim < args.min_similarity:
					skip=True
			if skip: continue

		
		# print seqs in copy-pasteable format 
		seqs = MSAs[insertion]
		print("-------------")
		for species in seqs.keys():
			print('> {}'.format(species))
			print(seqs[species]+'\n')
		print("-------------")
		print(insertion)
		return

if __name__ == '__main__':
	main()