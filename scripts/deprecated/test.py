#!/usr/bin/python
import os
# from pathlib import Path

# treeName = 'abc123'
# with open('config.txt', 'w') as out:
# 	phylo_dir = os.path.dirname(os.path.realpath(__file__))
# 	out.write('phylo_dir={}/\n'.format(phylo_dir))
# 	out.write('tree_dir={}/{}\n'.format(phylo_dir, treeName))
# def get_directories():
# 	with open('config.txt', 'r') as cfg:
# 		directories = []
# 		for line in cfg:
# 			directories.append(line.split('=')[1].rstrip())
# 	return directories[0], directories[1]

# p_dir, t_dir = get_directories()
# print('p_dir: {}'.format(p_dir))
# print('t_dir: {}'.format(t_dir))
def load_phylo_directory():
	with open('config.txt', 'r') as cfg:
		line = cfg.readline()
		phylodir = line.split('=')[1].rstrip()
	return phylodir
print(load_phylo_directory())