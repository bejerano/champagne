#!/usr/bin/python3.4

'''
scripts/convertEvidenceToNexus.py paenungulata-MSA-final-hg38-2019-07-28 0.6 0.6
scripts/convertEvidenceToNexus.py primates-MSA-final-mm10-2019-07-26 0.6 0.6
'''

import sys
from collections import defaultdict

OUTGROUP = None
tree_name = sys.argv[1]
tree_dir = '/cluster/u/jahemker/phylo2/trees/' + tree_name + '/'
ithreshold = float(sys.argv[2])
dthreshold = float(sys.argv[3])
# evidence = defaultdict(list)
nexus_evidence = defaultdict(list)
ntax = 0
nchar = 0

# -- 1. Load evidence --

evidence = [line.split() for line in open(tree_dir+'evidence.txt', 'r').readlines()]

# -- 2. Filter evidence --

evidence_at_threshold = []
for case in evidence:
	statuses = case[5:]
	case_type = case[0]
	case_at_threshold = case[:5]

	for status in statuses:
		# reference
		if ':' not in status:
			case_at_threshold.append(status)
			OUTGROUP = status[:-1]

		# regular
		else:
			stat, score = status.split(':')
			
			# replace sub-threshold score with ?
			if score == '~':
				case_at_threshold.append(stat)
				continue

			score = float(score)
			if case_type == 'insertion' and score < ithreshold:
				case_at_threshold.append(stat[:-1]+'?')
			elif case_type == 'deletion' and score < dthreshold:
				case_at_threshold.append(stat[:-1]+'?')
			else:
				case_at_threshold.append(stat)

	evidence_at_threshold.append(case_at_threshold)

# -- 3. Convert to Nexus -- 

with open('/cluster/u/jahemker/phylo2/paup-evidence/'+tree_name+'.nex', 'w') as out:
	out.write('#nexus\n\n')
	out.write('begin data;\n')
	for case in evidence_at_threshold:
		nchar += 1
		type_ = case[0]
		for outcome in case[5:]:
			# outcome = outcome.split(':')[0]
			species, verdict = outcome[:-1], outcome[-1:]	
			if type_ == 'deletion':
				if verdict == '-': nexus_evidence[species].append('1')
				elif verdict == '+': nexus_evidence[species].append('0')
				elif verdict == '?': nexus_evidence[species].append('?')
			elif type_ == 'insertion':
				if verdict == '+': nexus_evidence[species].append('1')
				elif verdict == '-': nexus_evidence[species].append('0')
				elif verdict == '?': nexus_evidence[species].append('?')
	ntax = len(list(nexus_evidence.keys()))
	out.write('dimensions ntax={} nchar={};\nmatrix\n'.format(ntax, nchar))
	for species in nexus_evidence.keys():
		out.write(species + ' ' + ''.join(nexus_evidence[species]) + '\n')
	out.write(';\nend;\n')

	out.write('begin paup;\n')
	out.write('outgroup {};\n'.format(OUTGROUP))
	out.write('end;\n')
	print("There were {} total characters.".format(nchar))

##############
# print("Converting evidence for tree {}.".format(treeName))
# with open('/cluster/u/jahemker/phylo2/trees/'+treeName+'/topTreesEvidence/IT={}-DT={}.txt'.format(ithreshold, dthreshold), 'r') as infile:
# 	with open('/cluster/u/jahemker/phylo2/paup-evidence/'+treeName+'.nex', 'w') as out:
# 		out.write('#nexus\n\n')
# 		out.write('begin data;\n')
# 		for line in infile.readlines():
# 			nchar += 1
# 			line = line.split()
# 			print(line)
# 			exit()
# 			type_ = line[0]
# 			for outcome in line[5:]:
# 				outcome = outcome.split(':')[0]
# 				species, verdict = outcome[:-1], outcome[-1:]	
# 				if type_ == 'deletion':
# 					if verdict == '-': evidence[species].append('1')
# 					elif verdict == '+': evidence[species].append('0')
# 					elif verdict == '?': evidence[species].append('?')
# 				elif type_ == 'insertion':
# 					if verdict == '+': evidence[species].append('1')
# 					elif verdict == '-': evidence[species].append('0')
# 					elif verdict == '?': evidence[species].append('?')
# 		ntax = len(list(evidence.keys()))
# 		out.write('dimensions ntax={} nchar={};\nmatrix\n'.format(ntax, nchar))
# 		for species in evidence.keys():
# 			out.write(species + ' ' + ''.join(evidence[species]) + '\n')
# 		out.write(';\nend;\n')

# 		out.write('begin paup;\n')
# 		out.write('outgroup {};\n'.format(OUTGROUP))
# 		out.write('end;\n')
# 		print("There were {} total characters.".format(nchar))
print("Converted to NEXUS format. Output in paup-evidence/{}.nex.".format(tree_name))



