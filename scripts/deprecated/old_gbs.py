#!/usr/bin/python3.4
import os, sys
import argparse
from io import StringIO
from Bio import Phylo
from collections import defaultdict


"""
get_branch_scores.py
args (in order):
-n, newick-tree: tree in Newick format (no spaces)
-t, tree-name: name of tree directory (for evidence)
-i, insertion-threshold
-d, deletion-threshold
useful trees:
(((speTri2,marMar2),(cavPor3,hetGla2)),(mm10,rn6))
useful tree names:
squirrel-MSA-final-hg38-2019-07-24
useful thresholds:
squirrel: 0.6 0.6
useful commands:
Squirrel:
scripts/get_branch_scores.py -n '(((speTri2,marMar2),(cavPor3,hetGla2)),(mm10,rn6))' -t squirrel-MSA-final-hg38-2019-07-24 -i 0.6 -d 0.6
scripts/get_branch_scores.py -n '(((speTri2,marMar2),(mm10,rn6)),(cavPor3,hetGla2))' -t squirrel-MSA-final-hg38-2019-07-24 -i 0.6 -d 0.6
scripts/get_branch_scores.py -n '(((cavPor3,hetGla2),(mm10,rn6)),(speTri2,marMar2))' -t squirrel-MSA-final-hg38-2019-07-24 -i 0.6 -d 0.6
scripts/get_branch_scores.py -n '((speTri2,cavPor3),mm10)' -t rodents-triplet-MSA-hg38-2019-10-02 -i 0.6 -d 0.6 -o hg38
rodents-triplet-MSA-hg38-2019-10-02
Paenungulata:
scripts/get_branch_scores.py -n '((triMan1,loxAfr3),proCap02)' -t paenungulata-MSA-final-hg38-2019-07-28 -i 0.6 -d 0.6
scripts/get_branch_scores.py -n '((proCap02,loxAfr3),triMan1)' -t paenungulata-MSA-final-hg38-2019-07-28 -i 0.6 -d 0.6
scripts/get_branch_scores.py -n '((proCap02,triMan1),loxAfr3)' -t paenungulata-MSA-final-hg38-2019-07-28 -i 0.6 -d 0.6
Primates:
scripts/get_branch_scores.py -n '((((((gorGor5,(panTro6,hg38)),ponAbe3),rheMac8),calJac3),tarSyr2),(micMur3,otoGar3))' -t primates-MSA-final-mm10-2019-07-26 -i 0.675 -d 0.675 -o mm10
scripts/get_branch_scores.py -n '((((((gorGor5,(panTro6,hg38)),ponAbe3),rheMac8),calJac3),tarSyr2),(micMur3,otoGar3))' -t primates-MSA-final-mm10-2019-07-26 -i 0.65 -d 0.65 -o mm10
Triplets:
scripts/get_branch_scores.py -n '((susScr3,bosTau8),canFam3)' -t PCD-MSA-final-hg38-2019-07-29 -i 0.65 -d 0.625 -o hg38
scripts/get_branch_scores.py -n '((turTru2,bosTau8),equCab2)' -t DCH-MSA-final-hg38-2019-08-10 -i 0.65 -d 0.6 -o hg38
scripts/get_branch_scores.py -n '((canFam3,felCat8),susScr3)' -t DCP-MSA-final-hg38-2019-07-31 -i 0.65 -d 0.6 -o hg38
scripts/get_branch_scores.py -n '((mm10,rn6),cavPor3)' -t MRG-MSA-final-oryCun2-2019-08-01 -i 0.625 -d 0.6 -o oryCun2
scripts/get_branch_scores.py -n '((pteVam1,myoLuc2),canFam3)' -t MMD-MSA-final-hg38-2019-08-10 -i 0.7 -d 0.625 -o hg38
scripts/get_branch_scores.py -n '((hg38,mm10),canFam3)' -t HMD-MSA-final-loxAfr3-2019-07-28 -i 0.65 -d 0.6 -o loxAfr3
"""

parser = argparse.ArgumentParser()
parser.add_argument("-n", "--newick_tree",
					required=True)
parser.add_argument("-t", "--tree_name",
					required=True)
parser.add_argument("-i", "--ithreshold",
					type=float,
					required=True)
parser.add_argument("-d", "--dthreshold",
					type=float,
					required=True)
parser.add_argument('-o', "--outgroup",
					required=True)
parser.add_argument('-m', "--minSize",
					type=int,
					nargs='?',
					const=50,
					required=True,
                    help="Minimum insertion/deletion size. Default is 50.")
args = parser.parse_args()

def main():

	# -- 0. Init params --

	newick_tree = args.newick_tree
	tree_name = args.tree_name
	ithreshold = args.ithreshold
	dthreshold = args.dthreshold
	outgroup = args.outgroup
	minSize = args.minSize

	tree_dir = '/cluster/u/jahemker/phylo2/trees/'+tree_name+'/'
	PHYLO_DIR = '/cluster/u/jahemker/phylo2/'
	# PHYLO_DIR, tree_dir = get_directories()
	nameMap = {line.split(':')[0].strip() : line.split(':')[1].strip() for line in open(PHYLO_DIR+'species/namemap.txt', 'r').readlines()}
	print("Calculating S and C for the following Newick tree: {}".format('\n'+newick_tree))

	# -- 1. Load evidence --

	evidence = list(set([line for line in open(tree_dir+'evidence.txt', 'r').readlines()]))
	evidence = [e.split() for e in evidence]
	# -- 2. Filter evidence --

	evidence_at_threshold = []
	unique_locations = []
	for case in evidence:
		statuses = case[5:]
		case_type = case[0]
		case_at_threshold = case[:5]
		unique_indel = ' '.join(case_at_threshold)
		if unique_indel in unique_locations:
			continue
		else:
			unique_locations.append(unique_indel)

		for status in statuses:
			# reference
			if ':' not in status:
				if case_type == 'deletion':
					case_at_threshold.append(status[:-1]+'-')
				else:
					case_at_threshold.append(status)
			# regular
			else:
				stat, score = status.split(':')
				
				# replace sub-threshold score with ?
				if score == '~':
					case_at_threshold.append(stat)
					continue

				score = float(score)
				if case_type == 'insertion' and score < ithreshold:
					case_at_threshold.append(stat[:-1]+'?')
				elif case_type == 'deletion' and score < dthreshold:
					case_at_threshold.append(stat[:-1]+'?')
				elif int(case[1]) < minSize: #This gets rid of any indels that are less than the specified minimum size
					case_at_threshold.append(stat[:-1]+'?')
				else:

					# Transform deletions
					if case_type == 'deletion':
						if stat[-1:] == '-':
							case_at_threshold.append(stat[:-1]+'+')
						elif stat[-1:] == '+':
							case_at_threshold.append(stat[:-1]+'-')
					else:
						case_at_threshold.append(stat)

		evidence_at_threshold.append(case_at_threshold)

	print("There are {} total cases to consider".format(len(evidence_at_threshold)))
	
	# Write this transformed evidence to a file
	with open(tree_dir+'evidence-at-{}-{}-{}.txt'.format(ithreshold, dthreshold, minSize), 'w') as out:
		for case in evidence_at_threshold:
			out.write(' '.join(case)+'\n')

	print("Saved transformed evidence at {}".format(tree_dir+'evidence-at-{}-{}-{}.txt'.format(ithreshold, dthreshold,minSize)))

	# -- 3. Convert to Nexus -- 
	
	nexus_evidence = defaultdict(list)
	ntax = 0
	nchar = 0
	with open('/cluster/u/jahemker/phylo2/old-paup-evidence/{}-{}-{}-{}-old.nex'.format(tree_name, ithreshold, dthreshold,minSize), 'w') as out:
		out.write('#nexus\n\n')
		out.write('begin data;\n')
		for case in evidence_at_threshold:
			nchar += 1
			type_ = case[0]
			for outcome in case[5:]:
				# outcome = outcome.split(':')[0]
				species, verdict = outcome[:-1], outcome[-1:]	
				if verdict == '+': nexus_evidence[species].append('1')
				elif verdict == '-': nexus_evidence[species].append('0')
				elif verdict == '?': nexus_evidence[species].append('?')

		ntax = len(list(nexus_evidence.keys()))
		out.write('dimensions ntax={} nchar={};\nmatrix\n'.format(ntax, nchar))
		for species in nexus_evidence.keys():
			out.write(species + ' ' + ''.join(nexus_evidence[species]) + '\n')
		out.write(';\nend;\n')

		out.write('begin paup;\n')
		out.write('outgroup {};\n'.format(outgroup))
		out.write('end;\n')
		print("There were {} total characters.".format(nchar))
	print("Saved Nexus evidence at /cluster/u/jahemker/phylo2/old-paup-evidence/{}-{}-{}-{}-old.nex".format(tree_name, ithreshold, dthreshold, minSize))

	# -- Iterate over nodes and calculate scores --
	subtrees = [newick_tree]
	species = newick_tree.replace('(', ' ').replace(')', ' ').replace(',', ' ').split()
	print("All relevant species: {}".format(species))

	it = 1
	while True:
		print("\n-- Node {} --".format(it))

		# Base case
		if len(subtrees)==0:
			print("Reached leaf.")
			return

		subtree = subtrees.pop(0)
		lengthened_subtree = subtree
		for name in nameMap.keys():
			lengthened_subtree = lengthened_subtree.replace(name, nameMap[name])

		handle = StringIO(lengthened_subtree)
		tree = Phylo.read(handle, "newick")
		Phylo.draw_ascii(tree)

		# Remove outer brackets
		subtree = subtree[1:-1]

		# Split into two nodes
		A, B = None, None
		n_open, n_closed = 0, 0
		for i, char in enumerate(subtree):
			if char == '(':
				n_open += 1
			elif char == ')':
				n_closed += 1
			elif char == ',' and n_open==n_closed:
				A, B = subtree[:i], subtree[i+1:]

		A_species = A.replace('(', ' ').replace(')', ' ').replace(',', ' ').split()
		B_species = B.replace('(', ' ').replace(')', ' ').replace(',', ' ').split()
		X = [s for s in species if s not in A_species+B_species]
		print("A is {}".format(A_species))
		print("B is {}".format(B_species))
		print("X is {}".format(X))

		# Prevents single species being added
		if len(A_species)>1:
			subtrees += [A]
		if len(B_species)>1:
			subtrees += [B]

		# Calculate S
		S_cases, C_cases = [], []

		# Write the standardized cases to a Nexus file

		for case in evidence_at_threshold:
			# Maps from species name to character state
			statuses = {stat[:-1]:stat[-1] for stat in case[5:]}

			# Check if S
			in_A, in_B, notin_X, skip = False, False, False, False
			for s in A_species:
				if statuses[s] == '+':
					in_A = True
				if statuses[s] == '-':
					skip = True
			for s in B_species:
				if statuses[s] == '+':
					in_B = True
				if statuses[s] == '-':
					skip = True
			for s in X:
				if statuses[s] == '-':
					notin_X = True
				if statuses[s] == '+':
					skip = True

			if (in_A and in_B and notin_X and (not skip)): 
				S_cases.append(case)

			# Check if C
			A_closer = False
			in_A, notin_B, in_X, skip = False, False, False, False
			for s in A_species:
				if statuses[s] == '+':
					in_A = True
				if statuses[s] == '-':
					skip = True
			for s in B_species:
				if statuses[s] == '-':
					notin_B = True
				if statuses[s] == '+':
					skip = True
			for s in X:
				if statuses[s] == '+':
					in_X = True
				if statuses[s] == '-':
					skip = True
			
			if (in_A and notin_B and in_X and (not skip)): 
				A_closer = True

			B_closer = False
			notin_A, in_B, in_X, skip = False, False, False, False
			for s in A_species:
				if statuses[s] == '-':
					notin_A = True
				if statuses[s] == '+':
					skip = True
			for s in B_species:
				if statuses[s] == '+':
					in_B = True
				if statuses[s] == '-':
					skip = True
			for s in X:
				if statuses[s] == '+':
					in_X = True
				if statuses[s] == '-':
					skip = True

			if (notin_A and in_B and in_X and (not skip)): 
				B_closer = True

			assert((A_closer and B_closer) is False)
			if (A_closer or B_closer):
				C_cases.append(case)

		# Print scores
		print("\nAt this node, there were {} supporting cases and {} conflicting cases.".format(len(S_cases), len(C_cases)))
		show_cases = input("Want to see the cases?")
		S_cases = sorted(S_cases, key=lambda x: int(x[3]))

		S_cases = [' '.join(case) for case in S_cases]
		C_cases = [' '.join(case) for case in C_cases]


		if show_cases.startswith('y'):
			print("S cases:\n{}".format('\n'.join(S_cases)))
			print("C cases:\n{}".format('\n'.join(C_cases)))
	
		it+=1

if __name__ == '__main__':
	main()


