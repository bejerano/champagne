import os
import sys
import time
import pickle
import edlib
import subprocess
from Bio import SeqIO
from collections import defaultdict
from utils import *

MARGIN = 5
WINDOW = 25
SIMILARITY_STEP = 0.1

RGENOME_FILE = "hg38.fa"

'''
#########################################
###        deletion_is_absent         ###
#########################################

EXAMPLE TRUE CASE:
-------------
O+ M- D+ E-
-------------
'''
def deletion_is_absent(insertion, reference, rseq, query, qseq, chain, similarity_threshold, window_size):
	print("\nSearching {} to see if deletion is absent (-).".format(query))

	rcc = int(chain[0][5]) # reference-current-coordinate
	qcc = int(chain[0][10]) # query-current-coordinate
	ris, rie = int(insertion[4]), int(insertion[5]) # reference-insertion-start/end
	qip = None # query-insertion-position --> translation of ris to query coords

	r_chr = insertion[3]
	q_chr, q_chrom_size, q_strand = chain[0][7], int(chain[0][8]), chain[0][9]

	for line in chain[1:]:
		if len(line) < 3:
			break

		gapless = int(line[0]) # gapless-block-size
		qgap = int(line[1]) # query-gap-size
		rgap = int(line[2]) # reference-gap-size

		rcc, qcc = rcc+gapless, qcc+gapless

		# 1. Moved past site
		if rcc > rie+MARGIN: break

		# 2. Viable insertion found
		if (rcc >= ris-MARGIN and rcc <= ris+MARGIN
		  and rcc+qgap >= rie-MARGIN and rcc+qgap <= rie+MARGIN
		  and rgap == 0):

			window_similarity = get_window_similarity(window_size, ris, rie, qcc, qcc, rseq, q_strand, q_chrom_size, qseq)

			# ----------------------------------
			if window_similarity >= similarity_threshold:
				print("Deletion is absent: marking as (-).")
				return True, window_similarity
			break

		rcc, qcc = rcc+qgap, qcc+rgap

	print("Can't confidently say that deletion is absent.")
	return False, -1.0


'''
#########################################
###        deletion_is_present        ###
#########################################

EXAMPLE TRUE CASE:
-------------
O+ M- D+ E+
-------------
'''
def deletion_is_present(insertion, reference, rseq, query, qseq, chain, similarity_threshold, window_size):

	print("\nSearching {} to see if deletion is present (+).".format(query))
	indel, foundIn, insertionSize, rchrom, ris, rie, __, __, __, __ = insertion
	insertionSize = int(insertionSize)

	qcc = int(chain[0][10]) # q2 chain start
	query_chr, query_chrom_size, query_strand, query_start, query_end = chain[0][7], int(chain[0][8]), chain[0][9], None, None 
	rcc, ris, rie = int(chain[0][5]), int(ris), int(rie) # ref chain start

	insertionSeq = str(rseq[ris:rie].seq).lower()

	# ----------------------------------------------------
	# This is true when we've found the query start and 
	# are iterating through the insertion range
	getting_query_end = False
	steps = []
	for line in chain[1:]:
		if len(line) < 3:
			print "Reached end of chain, found nothing. RCC={}".format(rcc)
			continue

		gapless_block_size = int(line[0])
		query_gap_size = int(line[1])
		ref_gap_size = int(line[2])

		steps.append("rcc is {}bp before ris, gapless={}/qgap={}/rgap={}".format(ris-rcc, gapless_block_size, query_gap_size, ref_gap_size))

		# -- If we're looking for query_end, special behavior -----
		if getting_query_end is True:

			# The end of the insertion was within 
			# a query gap
			if rcc >= rie:
				query_end = qcc
				print "Got query end (RIS was in a query gap)."
				break

			rcc += gapless_block_size
			qcc += gapless_block_size

			# We've reached the end of the insertion
			if rcc >= rie:
				query_end = qcc - (rcc-rie)
				print "Got query end (RIS was in gapless)."
				break
			# We're still inside it
			else:
				# print "Still inside insertion range."
				if ref_gap_size > 10: 
					print "There's a {}bp double-sided gap at rcc={}, within the insertion range, so breaking.".format(ref_gap_size, rcc)
					# break

				rcc += query_gap_size
				qcc += ref_gap_size

		# ---------------------------------------------------------
		else:

			# If moved past site, end search
			if rcc > rie:
				print "Moved past site"
				break

			rcc += gapless_block_size
			qcc += gapless_block_size

			# 1. Whole insertion is contained in a perfect gapless block
			# if (hg38_curr_coord - gapless_block_size <= hg38_ins_start
			# 	and hg38_curr_coord >= hg38_ins_end):
			if (rcc >= rie + MARGIN):
				print "Whole insertion contained in perfect gapless block."
				print("\n".join(steps[-5:]))
				query_start = qcc - (rcc-ris)
				query_end = query_start + insertionSize
				break

			# 2. A gapless block ends a little way into the insertion
			if (rcc > ris
				and ref_gap_size == 0):
				# print "Gapless block ends a little way into the insertion range. Comparing with edlib."
				query_start = qcc - (rcc-ris)
				getting_query_end = True

			# 3. The insertion begins in the query gap in between the
			# previous gapless block and the next one
			if (rcc <= ris 
				and rcc + query_gap_size > ris
				and rcc + query_gap_size <= rie
				and ref_gap_size == 0):
				print "Gapless block ends a little way before the insertion range"
				query_start = qcc
				getting_query_end = True

			rcc += query_gap_size
			qcc += ref_gap_size

	# -------------------------
	# If a viable insertion wasn't found, continue
	if query_start is None or query_end is None: 
		print "Viable insertion wasn't found"
		return False, -1.0

	print "Viable insertion found, so comparing sequences."

	window_similarity = get_window_similarity(WINDOW, ris, rie, query_start, query_end, rseq, query_strand, query_chrom_size, qseq)
	print "Window similarity={}".format(window_similarity)
	# ------------------------

	similarity = get_similarity(insertionSeq, qseq, query_strand, query_chrom_size, query_start, query_end)
	print "Insertion similarity={}".format(similarity)

	# Instead of checking window and insertion separately, check together
	totalSize = insertionSize + 2*WINDOW
	combined_similarity = ((2*WINDOW)/float(totalSize))*window_similarity + (insertionSize/float(totalSize))*similarity
	print "Combined similarity={}".format(combined_similarity)
	
	if combined_similarity >= similarity_threshold:
		print('Deletion is present. Marking as (+).')
		return True, similarity

	print("Can't confidently say that deletion is present.")
	return False, similarity


'''
#########################################
###        insertion_is_absent        ###
#########################################

Used for producing step 3 output files.

EXAMPLE TRUE CASE:
-------------
O- M+ D+ E-
-------------
'''

def insertion_is_absent(insertion, reference, rseq, query, qseq, chain, similarity_threshold, window_size):

	rcc = int(chain[0][5]) # reference-current-coordinate
	qcc = int(chain[0][10]) # query-current-coordinate
	rip = int(insertion[4]) # reference-insertion-position
	qip = None # query-insertion-position --> translation of rip to query coords

	qchromsize, qstrand = int(chain[0][8]), chain[0][9]

	rgaps_in_range = []
	qgaps_in_range = []

	chain_length = len(chain)
	for i in range(1, chain_length):
		line = chain[i]
		if len(line) < 3:
			print "Reached end of chain, found nothing."
			continue

		gapless = int(line[0]) # gapless-block-size
		qgap = int(line[1]) # query-gap-size
		rgap = int(line[2]) # reference-gap-size

		# RIP was in the previous query gap
		if rcc >= rip and qip is None:
			qip = qcc

		rcc, qcc = rcc+gapless, qcc+gapless 

		# RIP is in a gapless block
		if rcc >= rip and qip is None:
			qip = qcc - (rcc-rip)

		if rcc >= rip-25 and rcc <= rip+25:
			# print "rcc is now {} ahead of rip, so adding rgap.".format(rcc-rip)
			rgaps_in_range.append(rgap)
			qgaps_in_range.append(qgap)

		if rcc > rip+25:
			# print "rcc is now {} ahead of rip, so breaking.".format(rcc-rip)
			break

		rcc, qcc = rcc+qgap, qcc+rgap

	# print "In the 50bp window surrounding RIP, the reference gaps were of size: [{}]".format(", ".join([str(rgap) for rgap in rgaps_in_range]))

	'''
	We accept evidence if it passes two conditions: 
	1. There is a cumulative <15bp of gaps in the insertion window
	2. The sequences in the insertion window are sufficiently similar
	'''
	if qip is None: return False, -1.0

	window_similarity = get_window_similarity(window_size, rip, rip, qip, qip, rseq, qstrand, qchromsize, qseq)
	# print "Window similarity is {}".format(window_similarity)

	if sum(rgaps_in_range + qgaps_in_range) < 15 and window_similarity > similarity_threshold:
		return True, window_similarity
	return False, window_similarity

'''
#########################################
###    ref_deletion_find_insertion    ###
#########################################

Used for producing step 2 output files.

EXAMPLE CASE:
-------------
H- M+ D+
'''
def insertion_is_present(insertion, reference, rchromseq, q1, q1chromseq, q2, q2chromseq, chain, chromsizes, similarity_threshold, window_size):

	rcc = int(chain[0][5]) # reference-current-coordinate
	rip = int(insertion[4]) # reference-insertion-position
	insertionSize = int(insertion[2])

	### GET Q1 SEQUENCE ###
	q1chrom, q1strand, q1start, q1end = insertion[6], insertion[9], int(insertion[7]), int(insertion[8])
	q1chromsize = chromsizes[q1][q1chrom]				
	q1seq = get_seq_from_browser_coords(q1start, q1end, q1strand, q1chromsize, q1chromseq)

	### GET Q2 SEQUENCE ###
	q2cc = int(chain[0][10]) # query-current-coordinate
	q2chrom, q2chromsize, q2strand, q2start, q2end = chain[0][7], int(chain[0][8]), chain[0][9], None, None 

	for i in range(1, len(chain)):
		line = chain[i]
		if len(line) < 3: continue

		gapless = int(line[0])
		qgap = int(line[1])
		rgap = int(line[2])

		rcc, q2cc = rcc+gapless, q2cc+gapless

		# Case 1: viable single-sided insertion found
		if (rcc + MARGIN >= rip 
			and rcc - MARGIN <= rip
			and qgap == 0
			and rgap > 0):

			if qgap > 0: break

			q2start = q2cc + (rip-rcc)
			q2end = q2start + rgap
			break

		rcc, q2cc = rcc+qgap, q2cc+rgap

		# If past insertion range, break
		if rcc > rip + 2*MARGIN: break

	#######################
	if q2start is None or q2end is None: return False, -1.0

	window_similarity = get_window_similarity(window_size, rip, rip, q2start, q2end, rchromseq, q2strand, q2chromsize, q2chromseq)

	similarity = get_similarity(q1seq, q2chromseq, q2strand, q2chromsize, q2start, q2end)
	# print "Similarity is {}".format(similarity)

	totalSize = insertionSize + 2*WINDOW
	combined_similarity = ((2*WINDOW)/float(totalSize))*window_similarity + (insertionSize/float(totalSize))*similarity
	
	if combined_similarity >= similarity_threshold:
		return True, similarity

	return False, similarity
