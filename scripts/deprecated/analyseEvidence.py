#!/usr/bin/python

import os
import argparse
import matplotlib
import numpy as np
from matplotlib import pyplot as plt

parser = argparse.ArgumentParser()
parser.add_argument("-t", "--treename",
                    required=True,
                    help="Name of relevant tree directory.")
args = parser.parse_args()

def main():

	print('Processing evidence from tree: {}.'.format(args.treename))

	treedir = '/cluster/u/jahemker/phylo2/trees/{}/'.format(args.treename)
	casedir = treedir + 'cases/'

	all_evidence = [line.split() for line in open(treedir+'evidence.txt', 'r').readlines()]

	# extract supporting evidence
	supporting_evidence, conflicting_evidence = set([]), set([])
	for file in os.listdir(casedir):
		if 'S' in file:
			for line in open(casedir+file, 'r').readlines():
				if line.strip()=='': continue
				if line[0:3] not in ['del', 'ins']: continue
				supporting_evidence.add(line.strip())

		elif 'C' in file:
			for line in open(casedir+file, 'r').readlines():
				if line.strip()=='': continue
				if line[0:3] not in ['del', 'ins']: continue
				conflicting_evidence.add(line.strip())


	# make histograms (size, S versus C)
	supporting_insertions = [int(line.split()[1]) for line in list(supporting_evidence) if line.split()[0] == 'insertion']
	supporting_deletions = [int(line.split()[1]) for line in list(supporting_evidence) if line.split()[0] == 'deletion']
	conflicting_insertions = [int(line.split()[1]) for line in list(conflicting_evidence) if line.split()[0] == 'insertion']
	conflicting_deletions = [int(line.split()[1]) for line in list(conflicting_evidence) if line.split()[0] == 'deletion']


	print("Mean size of supporting insertions is {}, median {}, min {}, std {}.".format(np.mean(supporting_insertions), np.median(supporting_insertions), min(supporting_insertions), np.std(supporting_insertions)))
	print("Mean size of conflicting insertions is {}, median {}, min {}, std {}.".format(np.mean(conflicting_insertions), np.median(conflicting_insertions), min(conflicting_insertions), np.std(conflicting_insertions)))

	plt.hist(supporting_insertions, bins=50, alpha=0.5, color='green')
	plt.hist(conflicting_insertions, bins=50, alpha=0.5, color='blue')
	plt.show()
	plt.clf()

	print("Mean size of supporting deletions is {}, median {}, min {}, std {}.".format(np.mean(supporting_deletions), np.median(supporting_deletions), min(supporting_deletions), np.std(supporting_deletions)))
	print("Mean size of conflicting deletions is {}, median {}, min {}, std {}.".format(np.mean(conflicting_deletions), np.median(conflicting_deletions), min(conflicting_deletions), np.std(conflicting_deletions)))
	plt.hist(supporting_deletions, bins=50, alpha=0.5, color='green')
	plt.hist(conflicting_deletions, bins=50, alpha=0.5, color='blue')
	plt.show()

if __name__ == '__main__':
	main()