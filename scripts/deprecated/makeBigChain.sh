#!/bin/bash

if [ $# -ne 3 ]; then
    echo "Please supply the required arguments.";
    exit 1;
fi

chain=$1
assembly=$2
outdir=$3
outNameRoot=$(basename $chain .all.chain)

echo $chain
echo $assembly
echo $outNameRoot

mkdir -p $outdir/$outNameRoot

hgLoadChain -noBin -test $assembly bigChain $chain



