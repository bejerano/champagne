#!/usr/bin/python3

def selectEvidence(indels):
	best, bestScore = None, -1
	for indel in indels:
		speciesStatuses = [species[-1:] for species in indel[4:]]
		score = sum([1 for s in speciesStatuses if s != '?'])
		if score>bestScore: 
			best, bestScore = indel, score
	return best


def clusterNearbyInsertions(insertions):
	compressedInsertions = []
	currInterval = []
	currChrom, currStart = None, None

	for insertion in insertions:
		chrom, start = insertion[1], int(insertion[2])

		# Initialize
		if currChrom is None:
			currChrom, currStart = chrom, start

		distanceFromIntervalStart = start - currStart
		if distanceFromIntervalStart > 30 or chrom != currChrom:
			compressedInsertions.append(selectEvidence(currInterval))
			currChrom, currStart, currInterval = chrom, start, []
		
		currInterval.append(insertion)

	# Add last interval
	if len(currInterval) > 0:
		compressedInsertions.append(selectEvidence(currInterval))

	return compressedInsertions


def main():

	insertions = ["insertion chr9 59254448 59254448 cavPor3? marMar2+ mm10- oryCun2- rn6? speTri2+",
	"insertion chr9 59284361 59284361 cavPor3- marMar2+ mm10? oryCun2- rn6? speTri2+",
	"insertion chr9 59285104 59285104 cavPor3? marMar2+ mm10? oryCun2- rn6- speTri2+",
	"insertion chr9 59312133 59312133 cavPor3- marMar2+ mm10? oryCun2- rn6? speTri2+",
	"insertion chr9 59312715 59312715 cavPor3? marMar2+ mm10? oryCun2- rn6- speTri2+",
	"insertion chr10 59318858 59318858 cavPor3- marMar2+ mm10? oryCun2- rn6? speTri2+",
	"insertion chr10 59318858 59318858 cavPor3- marMar2+ mm10? oryCun2- rn6? speTri2+",
	"insertion chr10 59318838 59318858 cavPor3- marMar2+ mm10? oryCun2- rn6? speTri2+",
	"insertion chr10 59318878 59318858 cavPor3- marMar2+ mm10? oryCun2- rn6? speTri2+",
	"insertion chr10 59318858 59318858 cavPor3- marMar2+ mm10? oryCun2- rn6? speTri2+",
	"insertion chr9 59318858 59318858 cavPor3- marMar2+ mm10? oryCun2- rn6? speTri2+",
	"insertion chr9 59318853 59318858 cavPor3- marMar2+ mm10? oryCun2- rn6? speTri2+",
	"insertion chr9 59318859 59318858 cavPor3- marMar2+ mm10+ oryCun2- rn6? speTri2+",
	"insertion chr9 59318859 59318858 cavPor3- marMar2+ mm10+ oryCun2- rn6? speTri2+",
	"insertion chr9 59318859 59318858 cavPor3- marMar2+ mm10+ oryCun2- rn6? speTri2+",
	"insertion chr9 59318859 59318858 cavPor3- marMar2+ mm10+ oryCun2- rn6? speTri2+",
	"insertion chr9 59318859 59318858 cavPor3- marMar2+ mm10+ oryCun2- rn6? speTri2+",
	"insertion chr9 59318859 59318858 cavPor3- marMar2+ mm10+ oryCun2- rn6? speTri2+",
	"insertion chr10 59318858 59318858 cavPor3- marMar2+ mm10? oryCun2- rn6? speTri2+",
	]

	insertions = [x.split() for x in insertions]
	insertions = sorted(insertions, key = lambda x: (int(x[1][3:]), int(x[2])))
	print('BEFORE:')
	print('\n'.join([" ".join(x) for x in insertions]))
	results = clusterNearbyInsertions(insertions)
	print('AFTER:')
	print('\n'.join([" ".join(x) for x in results]))


if __name__ == '__main__':
	main()