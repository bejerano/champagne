#!/usr/bin/python3.4

import sys, os
import re
import argparse
from collections import defaultdict

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--evidence_path",
					required=True,
					help="Path to evidence.")
parser.add_argument("-c", "--chromosome",
					required=True,
					help="Chromosome to consider.")
args = parser.parse_args()

###
# File path format:
# /cluster/u/jahemker/phylo2/trees/pig-cow-dog-dynamic-hg38-2019-04-02/topTreesEvidence/IT=0.7-DT=0.6.txt
#
###

# Gives back a dictionary mapping from the order in the chromosome to
# a list of tuples, one for each species, of the format (chainID, chromosome, ref_chain_start, ref_chain_end)
def get_info(genes):
	treePath = "/".join(args.evidence_path.split('/')[:-2])
	logPath = treePath + '/log/'

	mapping = defaultdict(list)
	chromosome_info = defaultdict(dict)
	chain_info = defaultdict(dict)

	# Sort genes in order of their coordinates
	genes_in_order = sorted(genes.keys(), key = lambda x: genes[x][0])

	# Get info on genes in order of their coordinates
	for i, gene in enumerate(genes_in_order, 1):
		with open(logPath+gene+'.txt', 'r') as f:
			lines = [line for line in f.readlines()]

			# Get chain and chromosome for each species for this gene
			for j, line in enumerate(lines):
				if line.startswith("Loading chain"):
					line = line.split()
					species = line[4][:-1]
					chainID = line[2]
					chromosome, ref_chain_start, ref_chain_end = lines[j+1].split()[12], int(lines[j+1].split()[10]), int(lines[j+1].split()[11])
					mapping[gene].append((species, chainID, chromosome, ref_chain_start, ref_chain_end))
	
	# Maps from species to an ordered list of tuples (chain, chrom, ref_start, ref_end)
	species_info = defaultdict(list)
	for gene in genes_in_order:

		# If this gene requires a new chain, add it to species_info
		for species_tup in mapping[gene]:
			species, chainID, chrom, ref_chain_start, ref_chain_end = species_tup
			if len(species_info[species])==0 or species_info[species][-1][0] != chainID:
				species_info[species].append((chainID, chrom, ref_chain_start, ref_chain_end))

	# Print out each species information
	for species in species_info.keys():
		print("{}:".format(species))
		print(species_info[species])



def source_evidence(evidence):
	treePath = "/".join(args.evidence_path.split('/')[:-2])
	fullEvidencePath = treePath + '/evidence/'
	genes = defaultdict(list)

	# Find corresponding reference gene for each piece of evidence
	i = 1
	for ev in evidence:
		found = False

		# Find corresponding gene
		for evidence_file in os.listdir(fullEvidencePath):
			if found: break
			with open(fullEvidencePath+evidence_file, 'r') as f:
				for line in f.readlines():
					if found: break
					if " ".join(ev[2:5]) in line:
						print("Found source for {}.".format(" ".join(ev[:5])))
						found=True

						# Add gene number to map, and record indel
						if evidence_file[:-4] not in genes:
							genes[evidence_file[:-4]].append(i)
							i+=1
						genes[evidence_file[:-4]].append(ev[:5])

	for gene in sorted(genes.keys(), key = lambda x: genes[x][0]):
		print("Gene {}, number {}:".format(gene, genes[gene][0]))
		for indel in genes[gene][1:]:
			print(" ".join(indel))
	return genes

def load_evidence():
	evidence = []
	with open(args.evidence_path, 'r') as f:
		for line in f.readlines():
			line = line.split()
			if line[2] == 'chr'+args.chromosome:
				evidence.append(line)

	first_start = min([int(ev[3]) for ev in evidence])
	last_end = max([int(ev[4]) for ev in evidence])
	mid = (first_start + last_end)/2.0

	# Returns indels sorted by start coordinate
	evidence_subset = sorted([ev for ev in evidence if int(ev[4])<85659187], key = lambda x: int(x[3]))
	print("Number of cases: {}".format(len(evidence_subset)))
	for case in evidence_subset: print(case)

	return evidence_subset

def main():
	print("Finding source for evidence from chromosome {}, in file {}.".format(args.chromosome, args.evidence_path))

	evidence = load_evidence()
	genes = source_evidence(evidence)
	mapping = get_info(genes)

if __name__ == '__main__':
	main()