#!/usr/bin/python3

import os
import subprocess

chains = [
('speTri2', 'hetGla2'),
('speTri2', 'marMar2')
]

basedir = '/cluster/u/jahemker/phylo2/'
trackdir = '/cluster/u/jahemker/phylo2/ucsc-tracks/'
jobdir = '/cluster/u/jahemker/phylo2/ucsc-tracks/tmp/'

jobname = 'chain-tracks-{}/'.format(hash("-".join(sorted(['-'.join(x) for x in chains]))))

if not os.path.isdir(jobdir+jobname+'scripts/'): os.makedirs(jobdir+jobname+'scripts/')

paradir = jobdir+jobname+'scripts/'

with open(jobdir+jobname+'job', 'w') as jobfile:
	for chain in chains:
		ref, query = chain[0], chain[1]
		chainfile = basedir+'species/{}/ref-{}/{}.{}.all.chain'.format(query, ref, ref, query)
		chromsizes = basedir+'species/{}/{}.chrom.sizes'.format(ref, ref)

		command = trackdir + 'chainToBrowser.sh {} {} {}'.format(chainfile, ref, chromsizes)

		scriptname = jobdir + jobname +'scripts/job_'+'-'.join(chain)+'.sh'
		with open(scriptname, 'w') as script:
			script.write('#!/bin/bash' + '\n')
			script.write('export PYTHON_EGG_CACHE=results' + '\n')
			script.write('export PYTHONPATH=$PYTHONPATH:/cluster/u/yatisht/aligner/comparative/try/lib64/python2.7/site-packages/' + '\n')
			script.write('cd /cluster/u/jahemker/phylo2/ucsc-tracks/ \n')
			script.write(command + '\n')
		jobfile.write(scriptname + '\n')

with open(jobdir+jobname+ 'command', 'w') as para_command:
	para_command.write('#!/bin/bash' + '\n')
	para_command.write('cd {}\n'.format(jobdir+jobname))
	para_command.write("chmod +x scripts/*" + '\n')
	para_command.write("para create job -ram=22g -cpu=1 -maxJob=20" + '\n')
	para_command.write("para push" + '\n')

subprocess.call('chmod +x {}command'.format(jobdir+jobname), shell=True)
subprocess.call("{}command".format(jobdir+jobname), shell=True)