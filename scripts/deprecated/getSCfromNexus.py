import os, sys
from collections import defaultdict

# EXAMPLE ARGS
# python getSCfromNexus.py nexus_file.nex homo_sapiens canis_familiaris,mus_muscularis,bos_taurus

def countEvidenceFor(s1, s2, evidence, nCases, outgroup):
	if outgroup in [s1, s2]: return
	print("Evidence for ({},{}):".format(s1, s2))
	supporting, conflicting = 0, 0
	positions = {s:0 for s in list(evidence.keys())}
	for i in range(nCases):

		# print(i)
		outOfClusterSpecies = [s for s in list(evidence.keys()) if s not in [s1, s2, outgroup]]
		# print("Non-pair species: {}".format(outOfClusterSpecies))
		# print("Non-pair status: {}".format(outOfClusterStatuses))

		status = {}
		for s in list(evidence.keys()):
			status[s] = evidence[s][positions[s]]
			if status[s] == '(':
				while evidence[s][positions[s]] != ')':
					positions[s] += 1
					status[s] += evidence[s][positions[s]]
				# print("{}:{}".format(s, status[s]))
			positions[s] += 1

		outOfClusterStatuses = [status[species] for species in outOfClusterSpecies]

		# Supporting
		if status[s1]==status[s2]:
			condition_met = False
			for stat in outOfClusterStatuses:
				if stat != status[s1] and stat == status[outgroup]:
					condition_met = True
			if condition_met:
				# print("Found S: {}:{}, {}:{}, {}:{}".format(s1, evidence[s1][i], s2, evidence[s2][i], outOfClusterSpecies[0], outOfClusterStatuses[0]))
				supporting+=1

		# Conflicting
		if status[s1]!=status[s2]:
			condition_met = False
			for stat in outOfClusterStatuses:
				if stat==status[s1] and status[s2]==status[outgroup]:
					condition_met = True
				elif stat==status[s2] and status[s1]==status[outgroup]:
					condition_met = True

			if condition_met:
				# print("Found C: {}:{}, {}:{}, {}:{}".format(s1, evidence[s1][i], s2, evidence[s2][i], outOfClusterSpecies[0], outOfClusterStatuses[0]))
				conflicting+=1

	print("We found {} supporting and {} conflicting evidence cases, with S/C={}.".format(supporting, conflicting, float(supporting)/conflicting))

def main():
	if len(sys.argv) < 2: 
		print("You have to provide a filename.")
		exit(0)

	filename = sys.argv[1]
	outgroup = ' '.join(sys.argv[2].split('_')).lower()
	querySpecies = sys.argv[3]+','+outgroup
	paup_path = '/Users/jamesschull/Desktop/research/BejeranoLab/phyloV2/PAUP/'
	print("Processing evidence from {}.".format(filename))

	querySpecies = [' '.join(s.split('_')).lower() for s in querySpecies.split(',')]
	print("Species: {}".format(', '.join(querySpecies)))

	nchars = None

	with open(paup_path+filename, 'r') as f:
		inMatrix = False
		linesOfEvidence = defaultdict(list)

		# Extract information from NEXUS matrix
		for line in f.readlines():
			line = line.strip()
			if line=='': continue
			if (line.split()[0].lower()=='dimensions'): 
				if line.split()[1].lower().startswith('nchar'):

					# REMOVE -1 FOR LIU ET AL
					nchars = int(line.split()[1][6:])
					print("NCHAR: {}".format(nchars))
				elif len(line.split()) > 2 and line.split()[2].lower().startswith('nchar'):
					nchars = int(line.split()[2][6:].strip())
					print("NCHAR: {}".format(nchars))

			# CHANGE TO ' ' for LIU ET AL
			header = line.split(' ')[0].strip().lower().replace("'", "")

			if header=='matrix': 
				inMatrix=True
				continue
			if inMatrix and header==';':
				break

			if inMatrix:
				# CHANGE TO ' ' for LIU ET AL
				line = [s.strip() for s in line.split(' ') if s!='']
				evidenceLine = line[1].strip()
				# print(evidenceLine)
				# exit()
				if header in querySpecies:
					linesOfEvidence[header].append(evidenceLine)

			else:
				if header=='dimensions':
					print("Dimensions: ")
					print(line)
				elif header=='format':
					print("Format: ")
					print(line)

		# Join evidence
		joinedEvidence, nEvidenceCases = {}, None
		for item in linesOfEvidence.items():
			combinedEvidence = ''.join(item[1])
			nEvidenceCases = len(combinedEvidence)
			joinedEvidence[item[0]] = combinedEvidence
			print("{}: {}".format(item[0], len(combinedEvidence)))

		# exit()

		# Get S/C from evidence
		print("\nThere are a total of {} cases of evidence, and {} taxa.".format(nchars, len(list(joinedEvidence.keys()))))
		for i in range(len(querySpecies)):
			for j in range(i+1, len(querySpecies)):
				countEvidenceFor(querySpecies[i].lower(), querySpecies[j].lower(), joinedEvidence, nchars, outgroup)

if __name__ == '__main__':
	main()





