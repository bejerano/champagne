from verificationAlgorithms import *
from searchGene import *

with open("/cluster/u/jahemker/phylo2/trees/rodentV_min75/evidence.txt", 'r') as f:
	lines = [line.strip() for line in f.readlines() if line.split()[0]=='insertion' and 'mm10+' in line.split()]
	for line in lines:
		line = line.split()
		status, sims = verify_indel(midInsertion, species, chains, sequences, chromsizes, similarityThreshold, windowSize)
