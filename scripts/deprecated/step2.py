#####################
###    step2.py   ###
#####################

# - Runs second step of pipeline, using reference/queryA chainfiles
#   to find insertions or verify deletions in queryB. 

# CONSIDERATIONS:
# --------------
# 1. Do we actually want to allow a little bit of junk, rather than only 
#    keeping perfectly clean deletions?
# --------------

# USAGE:
# --------------
# python find_evidence.py INPUT-FILE REF_SPECIES QUERY_SPECIES MODE SIMILARITY_THRESHOLD LINES_PER_JOB
# --------------

import os
import sys
import time
import pickle
import edlib
import subprocess
from Bio import SeqIO
from collections import defaultdict
from utils import get_similarity, get_sites_dict, get_chain_dict, convert_coords
from algorithms import ref_insertion_find_insertion, ref_deletion_find_insertion, ref_insertion_find_deletion, ref_deletion_find_deletion


def main():

	# CASE 1: --------------------------------------
	# Script has been called by a submitted job: run
	# find_evidence code.
	# ----------------------------------------------
	if sys.argv[1] == 'JOB':

		print "job!"

		query_species = sys.argv[2]
		sitesfile = sys.argv[3]
		chainfile = sys.argv[4]
		outfile = sys.argv[5]
		mode = sys.argv[6]

		start = int(sys.argv[7])
		end = int(sys.argv[8])
		
		similarity_threshold = float(sys.argv[9])

		print "sitesfile: {}, start: {}, end: {}, mode: {}".format(sitesfile, start, end, mode)

		sites_dict = get_sites_dict(sitesfile, start, end, mode)
		chain_dict = get_chain_dict(chainfile, sites_dict)

		if mode == 'di':
			q1 = sitesfile.split('/')[-1].split('.')[1][:-1]
			ref_deletion_find_insertion(q1, query_species, sites_dict, chain_dict, outfile, similarity_threshold)
		elif mode == 'dd':
			ref_deletion_find_deletion(query_species, sites_dict, chain_dict, outfile, similarity_threshold)
		elif mode == 'ii':
			ref_insertion_find_insertion(query_species, sites_dict, chain_dict, outfile, similarity_threshold)
		elif mode == 'id':
			ref_insertion_find_deletion(query_species, sites_dict, chain_dict, outfile, similarity_threshold)


	# CASE 2: -----------------------------------------
	# Script has been called by user: create and submit 
	# job scripts via parasol. 
	# -------------------------------------------------
	else:

		sitesfile_base = sys.argv[1]
		reference_species = sys.argv[2]
		query_species = sys.argv[3]
		mode = sys.argv[4]
		query_mode = '-' if mode in ['dd', 'id'] else '+'
		similarity_threshold = float(sys.argv[5])

		sitesfile = '/cluster/u/jahemker/phylo2/outputs/sim{}/withID/{}.{}_ID.txt'.format(similarity_threshold, sitesfile_base[:-4], query_species)
		chainfile = '/cluster/u/jahemker/phylo2/chains/{}/{}.{}.all.chain'.format(query_species, reference_species, query_species)
		
		pre_sitesfile = '/cluster/u/jahemker/phylo2/outputs/sim{}/'.format(similarity_threshold) + sitesfile_base

		# 1. Label appropriate input file
		if sitesfile_base.count('.') > 2:
			print "Removing labels"
			pre_sitesfile = '/cluster/u/jahemker/phylo2/outputs/sim{}/'.format(similarity_threshold) + sitesfile_base
			awk_comm1 = "awk '{$10=\"\"; print}'"
			awk_comm2 = " {} | sort -k1,1n > {}.tmp ; mv {}.tmp {}".format(pre_sitesfile, pre_sitesfile, pre_sitesfile, pre_sitesfile)
			print awk_comm1
			print awk_comm2
			# awk_command = "awk '\{$10=''; print\}' {} | sort -k1,1n > {}.tmp ; mv {}.tmp {}".format(pre_sitesfile, pre_sitesfile, pre_sitesfile, pre_sitesfile)
			# print awk_command
			subprocess.call(awk_comm1 + awk_comm2, shell=True)
			print "submitted command"

		if not os.path.isdir("/cluster/u/jahemker/phylo2/outputs/sim{}/withID".format(similarity_threshold)):
			os.makedirs("/cluster/u/jahemker/phylo2/outputs/sim{}/withID".format(similarity_threshold))

		# remove IDs, join
		subprocess.call("join -1 1 -2 1 {} /cluster/u/jahemker/phylo2/chains/{}/{}.trimmed.chain_ids \
						| sort -k10n > {}".format(pre_sitesfile, query_species, query_species, sitesfile), shell=True)

		num_lines = sum(1 for line in open(sitesfile, 'r'))
		lines_per_job = int(sys.argv[6])

		# 2. Make output directory
		outdir = "/cluster/u/jahemker/phylo2/outputs/sim{}/{}.{}{}.{}LPJ".format(similarity_threshold, sitesfile_base[:-4], query_species, query_mode, lines_per_job)
		if not os.path.isdir(outdir):
			os.makedirs(outdir)

		# 3. Make job directory
		jobdir = "/cluster/u/jahemker/phylo2/jobs/sim{}/{}.{}{}.{}LPJ".format(similarity_threshold, sitesfile_base[:-4], query_species, query_mode, lines_per_job)
		if not os.path.isdir(jobdir + '/scripts'):
			os.makedirs(jobdir + '/scripts')

		# 4. Write jobs to job directory
		with open(jobdir + '/job', 'w') as jobfile:

			line_start, file_no = 1, 1
			line_end = line_start + lines_per_job - 1

			while line_start < num_lines:
				script_name = jobdir + '/scripts/job'+str(file_no)+'.sh'
				outfile = outdir + '/{}.{}{}.{}-{}.txt'.format(sitesfile_base[:-4], query_species, query_mode, line_start, line_end)

				with open(script_name, 'w') as f:
						f.write('#!/bin/sh' + '\n')
						f.write('export PYTHON_EGG_CACHE=results' + '\n')
						f.write('chmod g-wx,o-wx results' + '\n')
						f.write('export PYTHONPATH=$PYTHONPATH:/cluster/u/yatisht/aligner/comparative/try/lib64/python2.7/site-packages/' + '\n')
						f.write('python /cluster/u/jahemker/phylo2/scripts/step2.py JOB {} {} {} {} {} {} {} {}'.format(query_species, sitesfile, chainfile, outfile, mode, line_start, line_end, similarity_threshold) + '\n')

				jobfile.write(script_name + '\n')
				line_start = line_end + 1
				line_end = line_start + lines_per_job - 1
				file_no += 1

		print("Made job directory at phyloV2/jobs/sim{}/{}.".format(similarity_threshold, jobdir.split('/')[-1]))

		# 4. Write parasol command script
		with open(jobdir + '/command', 'w') as para_command:
			para_command.write('#!/bin/sh' + '\n')
			para_command.write('cd {}\n'.format(jobdir))
			para_command.write("chmod +x scripts/*" + '\n')
			para_command.write("para create job -ram=16g -maxJob=200" + '\n')
			para_command.write("para push" + '\n')
		print("Wrote command.")

		# 5. Submit jobs
		subprocess.call('chmod +x {}/command'.format(jobdir), shell=True)
		print("Changed permissions.")
		subprocess.call("{}/command".format(jobdir), shell=True)
		print("Submitted command.")

		print("\nJobs submitted. You'll find their output in phyloV2/outputs.")

	return


if __name__ == '__main__':
	main()