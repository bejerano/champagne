pragma solidity >=0.4.0 <0.6.0;

contract Bank {

	// Equivalent to hashmap: key is storage facility, value is capacity
	mapping(bytes32 => uint256) public waterInStorage;
	bytes32[] public storageFacilities;

	constructor(bytes32[] memory facilityNames) public {
		storageFacilities = facilityNames;
	}

	function waterStoredIn(bytes32 facility) view public returns (uint256) {
		require(validFacility(facility));
		return waterInStorage[facility];
	}

	function makeDepositIn(bytes32 facility) public {
		require(validFacility(facility));
		waterInStorage[facility] += 1;
	}

	function validFacility(bytes32 facility) view public returns (bool) {
		for (uint i=0; i<storageFacilities.length; i++) {
			if (storageFacilities[i] == facility) {
				return true;
			}
		}
		return false;
	}
}




