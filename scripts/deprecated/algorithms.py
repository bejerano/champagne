import os
import sys
import time
import pickle
import edlib
import subprocess
from Bio import SeqIO
from collections import defaultdict
from utils import *

MARGIN = 5
WINDOW = 25
SIMILARITY_STEP = 0.1

RGENOME_FILE = "hg38.fa"

'''
#########################################
###    ref_deletion_find_deletion     ###
#########################################

Used for producing step 3 output files.

EXAMPLE CASE:
-------------
H- M+ D+ E-
-------------
'''
'''
We have a problem where the dog insertion (indicating that we should reject it)
occurs sometimes a large way away from rip. We want to only accept the case
as a deletion if within a 15bp window either side of the insertion, we have no 
reference block of significant size. 

Approach 1: Keep track of ref gaps in the range {rip-15 <= rcc <= rip+15}. If there
is a single ref gap that is larger than 10bp, drop the result. 
'''

def ref_deletion_find_deletion(query_species, sites_dict, chain_dict, outfile, similarity_threshold):

	window_sims = open('/cluster/u/jahemker/phylo2/histograms/dd_sims', 'a')

	with open(outfile, 'w') as f:

		# 1. Load reference and query genomes
		r_genome = SeqIO.to_dict(SeqIO.parse('/cluster/u/jahemker/phylo2/wholegenomes/fasta/{}'.format(RGENOME_FILE), 'fasta'))
		q_genome = SeqIO.to_dict(SeqIO.parse('/cluster/u/jahemker/phylo2/wholegenomes/fasta/{}.fa'.format(query_species), 'fasta'))

		for chain_id in sites_dict.keys():
			chain = chain_dict.get(chain_id, None)
			if chain is None:
				print "Chain not found!"
				continue

			for siteNum, site in enumerate(sites_dict[chain_id]):
				print "\nChecking SITE: {}".format(' '.join(site))

				# # // for debugging
				# mm_chr, mm_strand, mm_start, mm_end = site[5], site[6], int(site[7]), int(site[8])
				# mm_chrom_size = mm_chrom_sizes[mm_chr]				
				# mm_start, mm_end = convert_coords(mm_start, mm_end, mm_chr, mm_strand, mm_chrom_size)
				# print "mm10 UCSC browser coordinates: {} {} {}".format(mm_chr, mm_start, mm_end)
				# # //

				rcc = int(chain[0][5]) # reference-current-coordinate
				qcc = int(chain[0][10]) # query-current-coordinate
				rip = int(site[3]) # reference-insertion-position
				qip = None # query-insertion-position --> translation of rip to query coords

				r_chr = site[2]
				q_chr, q_chrom_size, q_strand = chain[0][7], int(chain[0][8]), chain[0][9]
				print "loxAfr3 chain is for {} {}".format(q_chr, q_strand)

				rgaps_in_range = []
				qgaps_in_range = []

				chain_length = len(chain)
				for i in range(1, chain_length):
					line = chain[i]
					if len(line) < 3:
						print "Reached end of chain, found nothing."
						continue

					gapless = int(line[0]) # gapless-block-size
					qgap = int(line[1]) # query-gap-size
					rgap = int(line[2]) # reference-gap-size

					# RIP was in the previous query gap
					if rcc >= rip and qip is None:
						qip = qcc

					rcc, qcc = rcc+gapless, qcc+gapless 

					# RIP is in a gapless block
					if rcc >= rip and qip is None:
						qip = qcc - (rcc-rip)

					if rcc >= rip-WINDOW and rcc <= rip+WINDOW:
						print "rcc is now {} ahead of rip, so adding rgap.".format(rcc-rip)
						rgaps_in_range.append(rgap)
						qgaps_in_range.append(qgap)

					if rcc > rip+WINDOW:
						print "rcc is now {} ahead of rip, so breaking.".format(rcc-rip)
						break

					rcc, qcc = rcc+qgap, qcc+rgap

				print "In the 50bp window surrounding RIP, the reference gaps were of size: [{}]".format(", ".join([str(rgap) for rgap in rgaps_in_range]))

				'''
				We accept evidence if it passes two conditions: 
				1. There is a cumulative <15bp of gaps in the insertion window
				2. The sequences in the insertion window are sufficiently similar
				'''
				if qip is None: continue

				window_similarity = get_window_similarity(WINDOW, rip, rip, qip, qip, r_genome[r_chr], q_strand, q_chrom_size, q_genome[q_chr])
				print "Window similarity is {}".format(window_similarity)
				window_sims.write(str(window_similarity)+'\n')

				if sum(rgaps_in_range + qgaps_in_range) < 15 and window_similarity > similarity_threshold:
					print "Seems right--we'll keep it."
					f.write('\t'.join(site) + '\n')

	window_sims.close()


#########################################
###    ref_insertion_find_deletion    ###
#########################################
# HARDCODED
# 
# Used for producing step 3 output files.
#
# EXAMPLE CASE:
# -------------
# H+ M- D+ E-
def ref_insertion_find_deletion(query_species, sites_dict, chain_dict, outfile, similarity_threshold):

	window_sims = open('/cluster/u/jahemker/phylo2/histograms/step3-id-mm10', 'a+')
	with open(outfile, 'w') as f:

		r_genome = SeqIO.to_dict(SeqIO.parse('/cluster/u/jahemker/phylo2/wholegenomes/fasta/hg38.fa', 'fasta'))
		q_genome = SeqIO.to_dict(SeqIO.parse('/cluster/u/jahemker/phylo2/wholegenomes/fasta/loxAfr3.fa', 'fasta'))

		for chain_id in sites_dict.keys():
			chain = chain_dict.get(chain_id, None)
			if chain is None:
				print "Chain not found!"
				continue

			for siteNum, site in enumerate(sites_dict[chain_id]):
				print "\nChecking site: {}".format(' '.join(site))
				rcc = int(chain[0][5]) # reference-current-coordinate
				qcc = int(chain[0][10]) # query-current-coordinate
				ris, rie = int(site[3]), int(site[4]) # reference-insertion-start/end
				qip = None # query-insertion-position --> translation of ris to query coords

				r_chr = site[2]
				q_chr, q_chrom_size, q_strand = chain[0][7], int(chain[0][8]), chain[0][9]

				for line in chain[1:]:
					if len(line) < 3:
						print "Reached end of chain, found nothing."
						continue

					gapless = int(line[0]) # gapless-block-size
					qgap = int(line[1]) # query-gap-size
					rgap = int(line[2]) # reference-gap-size

					rcc, qcc = rcc+gapless, qcc+gapless

					# 1. Moved past site
					if rcc > rie+MARGIN: break

					# 2. Viable insertion found
					if (rcc >= ris-MARGIN and rcc <= ris+MARGIN
					  and rcc+qgap >= rie-MARGIN and rcc+qgap <= rie+MARGIN
					  and rgap == 0):

						print "Viable query deletion found"

						window_similarity = get_window_similarity(WINDOW, ris, rie, qcc, qcc, r_genome[r_chr], q_strand, q_chrom_size, q_genome[q_chr])
						window_sims.write(str(window_similarity)+'\n')

						# ----------------------------------
						if window_similarity >= similarity_threshold:
							print "Seems right, so we'll keep it."
							f.write('\t'.join(site) + '\n')
						break

					rcc, qcc = rcc+qgap, qcc+rgap

	window_sims.close()
	return

#########################################
###    ref_deletion_find_insertion    ###
#########################################
#
# Used for producing step 2 output files.
#
# EXAMPLE CASE:
# -------------
# H- M+ D+
def ref_deletion_find_insertion(q1, q2, sites_dict, chain_dict, outfile, similarity_threshold):

	window_sims = open('/cluster/u/jahemker/phylo2/histograms/di_sims', 'a')
	with open(outfile, 'w') as f:
		rgenome = SeqIO.to_dict(SeqIO.parse('/cluster/u/jahemker/phylo2/wholegenomes/fasta/{}'.format(RGENOME_FILE), 'fasta'))
		q1genome = SeqIO.to_dict(SeqIO.parse('/cluster/u/jahemker/phylo2/wholegenomes/fasta/{}.fa'.format(q1), 'fasta'))
		q2genome = SeqIO.to_dict(SeqIO.parse('/cluster/u/jahemker/phylo2/wholegenomes/fasta/{}.fa'.format(q2), 'fasta'))

		# Load chrom sizes for query 1
		q1chromsizes = get_chromsizes(q1)

		for chain_id in sites_dict.keys():
			chain = chain_dict.get(chain_id, None)
			if chain is None: continue

			for siteNum, site in enumerate(sites_dict[chain_id]):
				print '\n' + ('\t').join(site)
				rchrom = site[2]

				rcc = int(chain[0][5]) # reference-current-coordinate
				rip = int(site[3]) # reference-insertion-position
				insertion_size = int(site[1])

				### GET Q1 SEQUENCE ###
				q1chrom, q1strand, q1start, q1end = site[5], site[6], int(site[7]), int(site[8])
				q1chromsize = q1chromsizes[q1chrom]				
				q1seq = get_seq_from_browser_coords(q1start, q1end, q1strand, q1chromsize, q1genome[q1chrom])

				### GET Q2 SEQUENCE ###
				q2cc = int(chain[0][10]) # query-current-coordinate
				q2chrom, q2chromsize, q2strand, q2start, q2end = chain[0][7], int(chain[0][8]), chain[0][9], None, None 

				for i in range(1, len(chain[1:])+1):
					line = chain[i]
					if len(line) < 3: continue

					gapless = int(line[0])
					qgap = int(line[1])
					rgap = int(line[2])

					rcc, q2cc = rcc+gapless, q2cc+gapless

					# Case 1: viable single-sided insertion found
					if (rcc + MARGIN >= rip 
						and rcc - MARGIN <= rip
						and qgap == 0
						and rgap > 0):

						if qgap > 0: break

						q2start = q2cc + (rip-rcc)
						q2end = q2start + rgap
						break

					rcc, q2cc = rcc+qgap, q2cc+rgap

					# If past insertion range, break
					if rcc > rip + 2*MARGIN: break

				#######################
				if q2start is None or q2end is None: continue

				q2chromseq = q2genome[q2chrom]

				window_similarity = get_window_similarity(WINDOW, rip, rip, q2start, q2end, rgenome[rchrom], q2strand, q2chromsize, q2chromseq)
				window_sims.write(str(window_similarity)+ '\n')
				# ----------------------------------

				similarity = get_similarity(q1seq, q2chromseq, q2strand, q2chromsize, q2start, q2end)
				print "Similarity is {}".format(similarity)

				# Adjust threshold for smaller insertions
				adjusted_similarity = similarity_threshold+SIMILARITY_STEP if insertion_size <= 100 else similarity_threshold

				if similarity >= adjusted_similarity and window_similarity >= similarity_threshold:
					print "Keeping."
					f.write('\t'.join(site) + '\n')

	window_sims.close()
	return


#########################################
###    ref_insertion_find_insertion   ###
#########################################
#
# Used for producing step 2 output files.
#
# EXAMPLE CASE:
# -------------
# H+ M- D+

def ref_insertion_find_insertion(query_species, sites_dict, chain_dict, outfile, similarity_threshold):

	window_sims = open('/cluster/u/jahemker/phylo2/histograms/step2-ii-{}'.format(query_species), 'a+') 
	with open(outfile, 'w') as f:

		hg38_whole_genome = SeqIO.to_dict(SeqIO.parse('/cluster/u/jahemker/phylo2/wholegenomes/fasta/hg38.fa', 'fasta'))
		# query_whole_genome = SeqIO.to_dict(SeqIO.parse('/cluster/u/jahemker/phylo2/wholegenomes/fasta/{}.fa'.format(query_species), 'fasta'))

		for chain_id in sites_dict.keys():
			chain = chain_dict.get(chain_id, None)

			if chain is None:
				print "Chain not found!"
				continue

			query_chr_seq = SeqIO.read('/cluster/u/jahemker/phylo2/wholegenomes/fasta/{}_chroms/{}.fa'.format(query_species, chain[0][7]), 'fasta')
			# print "Loaded {} sequence for {}.".format(query_species, chain[0][7])

			for site in sites_dict[chain_id]:
				print "\nSITE: {}".format(' '.join(site))
				insertion_size = int(site[1])

				query_curr_coord = int(chain[0][10]) # q2 chain start
				query_chr, query_chrom_size, query_strand, query_start, query_end = chain[0][7], int(chain[0][8]), chain[0][9], None, None 

				hg38_curr_coord = int(chain[0][5]) # ref chain start
				hg38_chr, hg38_ins_start, hg38_ins_end = site[2], int(site[3]), int(site[4]) 
				hg38_seq = hg38_whole_genome[hg38_chr][hg38_ins_start:hg38_ins_end]
				hg38_seq = str(hg38_seq.seq).lower()

				# print "\nChecking {}bp insertion at (hg38) {}: {} {}".format(len(hg38_seq), hg38_chr, hg38_ins_start, hg38_ins_end)
				# print "Beginning of hg38 sequence: {}".format(hg38_seq[:10])

				# ----------------------------------------------------
				# This is true when we've found the query start and 
				# are iterating through the insertion range
				getting_query_end = False
				for line in chain[1:]:
					if len(line) < 3:
						print "Reached end of chain, found nothing."
						continue

					gapless_block_size = int(line[0])
					query_gap_size = int(line[1])
					ref_gap_size = int(line[2])

					# -- If we're looking for query_end, special behavior -----
					if getting_query_end is True:

						# The end of the insertion was within 
						# a query gap
						if hg38_curr_coord >= hg38_ins_end:
							query_end = query_curr_coord
							# print "Got query end (RIS was in a query gap)."
							break

						hg38_curr_coord += gapless_block_size
						query_curr_coord += gapless_block_size

						# We've reached the end of the insertion
						if hg38_curr_coord >= hg38_ins_end:
							query_end = query_curr_coord - (hg38_curr_coord-hg38_ins_end)
							# print "Got query end (RIS was in gapless)."
							break
						# We're still inside it
						else:
							# print "Still inside insertion range."
							if ref_gap_size > 10: 
								# print "There's a large double-sided gap at hg38 {}, so breaking.".format(hg38_curr_coord)
								break

							hg38_curr_coord += query_gap_size
							query_curr_coord += ref_gap_size
			
					# ---------------------------------------------------------
					else:

						# If moved past site, end search
						if hg38_curr_coord > hg38_ins_end:
							# print "Moved past site"
							break

						hg38_curr_coord += gapless_block_size
						query_curr_coord += gapless_block_size

						# 1. Whole insertion is contained in a perfect gapless block
						# if (hg38_curr_coord - gapless_block_size <= hg38_ins_start
						# 	and hg38_curr_coord >= hg38_ins_end):
						if (hg38_curr_coord >= hg38_ins_end):
							# print "Whole insertion contained in perfect gapless block."
							query_start = query_curr_coord - (hg38_curr_coord-hg38_ins_start)
							query_end = query_start + insertion_size
							break

						# 2. A gapless block ends a little way into the insertion
						if (hg38_curr_coord > hg38_ins_start
							and ref_gap_size == 0):
							# print "Gapless block ends a little way into the insertion range. Comparing with edlib."
							query_start = query_curr_coord - (hg38_curr_coord-hg38_ins_start)
							getting_query_end = True

						# 3. The insertion begins in the query gap in between the
						# previous gapless block and the next one
						if (hg38_curr_coord <= hg38_ins_start 
							and hg38_curr_coord + query_gap_size > hg38_ins_start
							and hg38_curr_coord + query_gap_size < hg38_ins_end
							and ref_gap_size == 0):
							# print "Gapless block ends a little way before the insertion range"
							query_start = query_curr_coord
							getting_query_end = True

						hg38_curr_coord += query_gap_size
						query_curr_coord += ref_gap_size

				# -------------------------
				# If a viable insertion wasn't found, continue
				if query_start is None or query_end is None: 
					print "Viable insertion wasn't found"
					continue

				print "Viable insertion found"

				window_similarity = get_window_similarity(WINDOW, hg38_ins_start, hg38_ins_end, query_start, query_end, hg38_whole_genome[hg38_chr], query_strand, query_chrom_size, query_chr_seq)
				window_sims.write(str(window_similarity) + '\n')
				# ------------------------

				similarity = get_similarity(hg38_seq, query_chr_seq, query_strand, query_chrom_size, query_start, query_end)
				print "Insertion similarity={}".format(similarity)

				# Adjust threshold for smaller insertions
				adjusted_similarity = similarity_threshold+SIMILARITY_STEP if insertion_size <= 100 else similarity_threshold

				if similarity >= adjusted_similarity and window_similarity >= similarity_threshold:
					f.write('\t'.join(site) + '\n')

	window_sims.close()
				
	return