#!/usr/bin/python3

# I REMOVED OCHPRI3 HG38 allCHAIN
import os, sys

phylopath = '/cluster/u/jahemker/phylo2/'
ref = sys.argv[1]

for entry in os.listdir(phylopath + 'species/'):
	if entry == 'hg38' or entry == 'namemap.txt': continue
	if entry not in ['mm10', 'rn6', 'criGri1', 'speTri2', 'cavPor3', 'hetGla2', 'mesAur1', 'marMar2']: continue
	speciesdir = phylopath + 'species/{}/'.format(entry)
	refdir = phylopath + 'species/{}/ref-{}/'.format(entry, ref)
	print("Modifying {}.".format(entry))
	if not os.path.isdir(refdir):
		os.makedirs(refdir)
	if not os.path.isdir(refdir + 'pickledChains'):
		os.makedirs(refdir + 'pickledChains')

	# REF-SPECIFIC FILES: all.chain, chain_ids, pickledChains
	for file in os.listdir(speciesdir):
		name = file
		file = speciesdir + name
		if (ref in name or 'chain' in name) and 'ref' not in name: os.rename(file, refdir+name)
			
