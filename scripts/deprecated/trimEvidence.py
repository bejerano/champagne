# Remove invalid evidence
import os
import sys
from sys import argv

evidenceFile = os.path.abspath(argv[1])
newEvidenceFile = evidenceFile + '.tmp'
print(evidenceFile)
print("New: {}".format(newEvidenceFile))
with open(newEvidenceFile, 'w') as f:
	for c, line in enumerate(open(evidenceFile, 'r').readlines()):
		# if c==10: break
		line = line.split()
		if line[0] == 'insertion':
			atLeastOneDeletion, numInsertions = False, 0
			for species in line[4:]:
				if species[:-1]=='oryCun2': continue
				if species[-1]=='-': atLeastOneDeletion = True
				if species[-1]=='+':numInsertions+=1
			if atLeastOneDeletion and numInsertions>=2: f.write(' '.join(line) + '\n')

		elif line[0] == 'deletion':
			atLeastOneInsertion, numDeletions = False, 0
			for species in line[4:]:
				if species[:-1]=='oryCun2': continue
				if species[-1]=='-': numDeletions+=1
				if species[-1]=='+': atLeastOneInsertion = True
			if atLeastOneInsertion and numDeletions>=2: f.write(' '.join(line) + '\n')