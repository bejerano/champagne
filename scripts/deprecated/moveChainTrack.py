#!/usr/bin/python3

import os
import subprocess

trackdir = '/cluster/u/jahemker/phylo2/ucsc-tracks'
wwwdir = '/cluster/www/trackHubWWW/jschullHub'
for filename in os.listdir('/cluster/u/jahemker/phylo2/ucsc-tracks'):
	if filename[-5:] == 'Track' and filename[0]!='.':
		orig_filename=filename
		filename = filename.split('.')
		ref, query = filename[0], filename[1]

		print("\nProcessing {}-{}.".format(ref, query))

		if not os.path.isdir('/cluster/www/trackHubWWW/jschullHub/{}'.format(ref)):
			os.makedirs('/cluster/www/trackHubWWW/jschullHub/{}'.format(ref))

		# Update genomes.txt
		genomes_path = '/'.join([wwwdir, 'genomes.txt'])
		genomes = [line.split()[1] for line in open(genomes_path, 'r').readlines() if line.startswith('genome')]
		if ref not in genomes:
			print("Reference not in genomes.txt, so updating it.")
			with open(genomes_path, 'a+') as genomes_txt:
				genomes_txt.write('genome {}\n'.format(ref))
				genomes_txt.write('trackDb {}/trackDb.txt\n\n'.format(ref))

		# Update trackDb.txt
		trackDb_path = '/'.join([wwwdir, ref, 'trackDb.txt'])

		text = '\
track {}_chainedTo_{} \n\
type bigChain \n\
bigDataUrl http://dev.stanford.edu/trackHubWWW/jschullHub/{}/{}.{}.bigChain.bb \n\
linkDataUrl http://dev.stanford.edu/trackHubWWW/jschullHub/{}/{}.{}.bigChain.link.bb \n\
shortLabel ({}) chain \n\
longLabel ({}) chained to ({}) \n\
visibility pack\n\n'.format(query, ref, 
						ref, ref, query,
						ref, ref, query,
						query,
						query, ref)

		if not os.path.exists(trackDb_path):
			print('No trackDb for this reference, so creating one.')
			with open(trackDb_path, 'w') as trackdb_txt:
				trackdb_txt.write(text)
		else:
			existing_tracks = [line.split()[1] for line in open(trackDb_path, 'r') if line.startswith('track')]
			if '{}_chainedTo_{}'.format(query, ref) not in existing_tracks:
				print('This chain not in trackDb, so adding it.')
				with open(trackDb_path, 'a') as trackdb_txt:
					trackdb_txt.write(text)
		
		# Copy files
		chainfile = '{}/{}/{}.{}.bigChain.bb'.format(wwwdir, ref, ref, query)
		linkfile = '{}/{}/{}.{}.bigChain.link.bb'.format(wwwdir, ref, ref, query)
		if not os.path.exists(chainfile):
			print("Copying bigChain.bb.")
			subprocess.call('scp {}/{}/bigChain.bb {}/{}/{}.{}.bigChain.bb'.format(trackdir, orig_filename, wwwdir, ref, ref, query), shell=True)
		if not os.path.exists(linkfile):
			print("Copying bigChain.link.bb.")
			subprocess.call('scp {}/{}/bigChain.link.bb {}/{}/{}.{}.bigChain.link.bb'.format(trackdir, orig_filename, wwwdir, ref, ref, query), shell=True)
