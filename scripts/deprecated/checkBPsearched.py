#!/usr/bin/python3

# Takes TREENAME as arg and prints the total number of bp searched

import sys, os

tree = sys.argv[1]
failed = []
total = 0

treepath = '/cluster/u/jahemker/phylo2/trees/{}/'.format(tree) 
if not os.path.exists(treepath): 
	print("That tree doesn't exist.")
	exit(0)
if os.path.exists(treepath + 'failed-no-sequence'):
	failed = list(os.listdir(treepath + 'failed-no-sequence'))

with open(treepath+'labelled_genes.txt') as f:
	lines = [line.split() for line in f.readlines()]
	for line in lines:
		if line[0] in failed: continue
		total += int(line[3]) - int(line[2])

print("We search a total of {}bp, or {}kbp, or {}Mbp.".format(total, round(total/float(1000)), round(total/float(1000000))))