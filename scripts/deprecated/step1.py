#####################
###    step1.py   ###
#####################

# USAGE
# python step1.py REFERENCE QUERY MODE N_LINES
#
# python find_indels.py hg38 mm10 i 50
# --> will search for insertions (hg38 +, mm10 -), processing 50
# lines of mm10.introns.bed per job.

# MODE is i (insertion) or d (deletion)
# insertion means insertion in reference (hg+ can-)
# deletion means deletion in reference (hg- can+)

# This improved persion takes more limited arguments, and will
# generate and submit job files itself. It does this at the cost
# of requiring a rigidly kept directory structure.

import os
import sys
import shutil
import subprocess
import time
import edlib
from Bio import SeqIO
import pickle
from collections import defaultdict
from subprocess import call
from utils import get_similarity, get_window_similarity, get_sites_dict, get_chain_dict, convert_coords

WINDOW=25
RGENOME_FILE = "hg38.fa"

# Primary function:
def find_indels(query_species, sites_dict, chain_dict, outfile, mode, target_col, non_target_col, similarity_threshold):

	r_genome = SeqIO.to_dict(SeqIO.parse('/cluster/u/jahemker/phylo2/wholegenomes/fasta/{}'.format(RGENOME_FILE), 'fasta'))
	q_genome = SeqIO.to_dict(SeqIO.parse('/cluster/u/jahemker/phylo2/wholegenomes/fasta/{}.fa'.format(query_species), 'fasta'))

	# initialize variables for para
	insertion_start_ref = -1
	insertion_start_query = -1
	insertion_end_ref = -1
	insertion_end_query = -1

	print "Writing results to {}.".format(outfile)

	out = open(outfile, 'w')
	window_sims = open('/cluster/u/jahemker/phylo2/histograms/step1-{}-{}-similarities.txt'.format(query_species, mode), 'a+')

	chain_ids = sites_dict.keys()
	num_chains = len(chain_ids)
	num_sites_found = 0

	# Only keep unique sites
	found_sites = set([])

	# Search each chain for indels
	for c, chain_id in enumerate(chain_ids, 1):
		print "Searching chain {} out of {}.".format(c, num_chains)
		chain = chain_dict.get(chain_id, None)

		if chain == None:
			print 'Chain not found!'
			continue

		ref_start = int(chain[0][5])
		query_start = int(chain[0][10])

		for site in sites_dict[chain_id]:
			print "\nSearching site: {}".format(' '.join(site))
			ensemblID = site[0]
			site_start = int(site[2])
			site_end = int(site[3])

			ref_offset = 0
			query_offset = 0

			r_chr = site[1]
			q_chr, q_chrom_size, q_strand = chain[0][7], int(chain[0][8]), chain[0][9]

			for i, line in enumerate(chain[1:], 1):
				if line == '': continue

				gapless_block_size = int(line[0])
				ref_offset += gapless_block_size
				query_offset += gapless_block_size

				# if we have stepped beyond the end of the site, break
				if ref_start + ref_offset > site_end:
					print "Reached end of site, so breaking."
					break

				# if we haven't reached the beginning of the site, skip line and update coords
				if ref_start + ref_offset < site_start:
					ref_offset += int(line[1]) if len(line) > 1 else 0
					query_offset += int(line[2]) if len(line) > 1 else 0
					continue

				# either insertion in reference or query
				insertion_size = int(line[target_col]) if len(line) > 1 else 0

				# if there is an insertion of >= 50bp, test similarity and record it
				if insertion_size >= 50 and line[non_target_col] == '0':

					insertion_start_ref = ref_start + ref_offset
					insertion_start_query = query_start + query_offset

					# insertion start/end is same coord for 'deletion' genome
					if mode=='i': #insertion
						insertion_end_ref = insertion_start_ref + insertion_size
						insertion_end_query = insertion_start_query
					elif mode=='d': #deletion
						insertion_end_ref = insertion_start_ref
						insertion_end_query = insertion_start_query + insertion_size

					print "\nrcoords: {} {} {}".format(r_chr, insertion_start_ref, insertion_end_ref)

					window_similarity = get_window_similarity(WINDOW, insertion_start_ref, insertion_end_ref, 
															  insertion_start_query, insertion_end_query, 
															  r_genome[r_chr], q_strand, q_chrom_size,
															  q_genome[q_chr])

					window_sims.write(str(window_similarity)+'\n')

					if window_similarity > similarity_threshold:
						print "Keeping."
						entry = ensemblID + '\t' + str(insertion_size) + '\t' + chain[0][2] + '\t' +  str(insertion_start_ref) + '\t' + str(insertion_end_ref)  + '\t' + chain[0][7] + '\t' + chain[0][9] + '\t' + str(insertion_start_query) + '\t' + str(insertion_end_query) + '\n'
						found_sites.add(entry)
						num_sites_found += 1

				ref_offset += int(line[1]) if len(line) > 1 else 0
				query_offset += int(line[2]) if len(line) > 1 else 0

		print "Search completed. {} sites found.".format(num_sites_found)

	# print "These are the unique sites found: '\n"
	for site in found_sites:
		out.write(site)

	out.close( )
	window_sims.close()

	return

# returns dictionary with (chainID : list of sites) items
def get_sites_dict(sitesfile, start, end):

	print "Loading sites."
	sites_dict = defaultdict(list)

	with open(sitesfile, 'r') as f:
		for lineNum, line in enumerate(f.readlines(), 1):
			if lineNum < start: continue
			if lineNum > end: break
			line = line.split()
			sites_dict[line[4]].append(line)

	print "Sites loaded. \n"
	return sites_dict

# returns dictionary of (chainID : chain) items
def get_chain_dict(chainfile, sites_dict):

	print "Loading chains."
	begin_chainload = time.time()

	# strings
	chainIDs = sites_dict.keys()
	# print "Num chain IDs: {}".format(len(chainIDs))
	# print "ChainIDs to load: {}.".format(chainIDs)

	maxID = str(max([int(chainID) for chainID in chainIDs]))
	# print "Max ID to load: {}.".format(maxID)

	minID = str(min([int(chainID) for chainID in chainIDs]))
	# print "Min ID to load: {}.".format(minID)

	foundFirstChain = False
	loadedAllChains = False

	with open(chainfile, 'r') as f:

		chain_dict = defaultdict(list)

		# Initialize 'current chain'
		curr_chain_id = -1
		curr_chain = []
		num_chains = 0

		for line in f.readlines():

			line = [word.strip() for word in line.split()]

			# ignore comments and blank line at end of each chain
			if len(line) == 0 or line[0].startswith('#'): continue

			################ Deal with line ################

			# Only start building dict once reached min chainID
			if foundFirstChain == False:
				# Set to true once we've reached our first relevant chain
				if line[0] == 'chain' and line[12] == minID:
					foundFirstChain = True
					curr_chain_id = line[12]
				else:
					continue

			# In relevant section of chain files
			else:
				if line[0] == 'chain':

					# Add loaded chain to dictionary
					chain_dict[curr_chain_id] = curr_chain
					curr_chain = []

					# If we've reached designated limit, open new file
					if curr_chain_id == maxID:
						loadedAllChains = True
						break

					curr_chain_id = line[12]

			curr_chain.append(line)

			################ Move to next line ################

		# Edge case: maxID is last ID in file
		if not loadedAllChains: chain_dict[curr_chain_id] = curr_chain

		print "Loaded chains in {} minutes.\n".format((time.time() - begin_chainload)/60)

		return chain_dict


def main():

	reference_species = sys.argv[1]
	query_species = sys.argv[2]
	mode = sys.argv[3]
	ref_mode = '+' if mode== 'i' else '-'
	query_mode = '+' if mode=='d' else '-'
	similarity_threshold = float(sys.argv[4])

	sitesfile = "/cluster/u/jahemker/phylo2/inputs/{}/introns.{}.bed".format(query_species, query_species)
	chainfile = "/cluster/u/jahemker/phylo2/chains/{}/{}.{}.all.chain".format(query_species, reference_species, query_species)

	# CASE 1: --------------------------------------
	# Script has been called by a submitted job: run
	# find_indels code.
	# ----------------------------------------------
	if len(sys.argv) > 7 and sys.argv[7] == 'JOB':

		print("Script called by job.")

		start = int(sys.argv[5])
		end = int(sys.argv[6])

		outdir = "/cluster/u/jahemker/phylo2/outputs/sim{}/{}{}.{}{}.{}LPJ".format(similarity_threshold, reference_species, ref_mode, query_species, query_mode, end - start + 1)
		if not os.path.isdir(outdir):
			os.makedirs(outdir)

		outfile = outdir + "/{}{}.{}{}.{}-{}.txt".format(reference_species, ref_mode, query_species, query_mode, start, end)

		print("Writing outputs to {}".format(outfile))

		# Initialize these - I can't remember why...
		# Something to do with parasol? Check emails.
		target_col = 1000
		non_target_col = 1000

		# contains zero-indexed column of gap we're looking for (in chainfile)
		if mode=='i':
			target_col = 1
			non_target_col = 2
		elif mode=='d':
			target_col = 2
			non_target_col = 1

		sites_dict = get_sites_dict(sitesfile, start, end)
		chain_dict = get_chain_dict(chainfile, sites_dict)

		find_indels(query_species, sites_dict, chain_dict, outfile, mode, target_col, non_target_col, similarity_threshold)

		return

	# CASE 2: -----------------------------------------
	# Script has been called by user: create and submit
	# job scripts via parasol.
	# -------------------------------------------------
	else:

		lines_per_job = int(sys.argv[5])
		ref_mode = '+' if mode== 'i' else '-'
		query_mode = '+' if mode=='d' else '-'

		jobdir = "/cluster/u/jahemker/phylo2/jobs/sim{}/{}{}.{}{}.{}LPJ".format(similarity_threshold, reference_species, ref_mode, query_species, query_mode, lines_per_job)

		num_lines = sum(1 for line in open(sitesfile, 'r'))

		print("{} lines in {}.".format(num_lines, sitesfile.split('/')[-1]))

		line_start = 1
		line_end = line_start + lines_per_job - 1
		file_no = 1

		if not os.path.isdir(jobdir + '/scripts'):
			os.makedirs(jobdir + '/scripts')

		with open(jobdir + '/job', 'w') as jobfile:

			while line_start < num_lines:

				script_name = jobdir + '/scripts/job'+str(file_no)+'.sh'

				with open(script_name, 'w') as f:
						f.write('#!/bin/sh' + '\n')
						f.write('export PYTHON_EGG_CACHE=results' + '\n')
						f.write('export PYTHONPATH=$PYTHONPATH:/cluster/u/yatisht/aligner/comparative/try/lib64/python2.7/site-packages/' + '\n')
						f.write('python /cluster/u/jahemker/phylo2/scripts/step1.py {} {} {} {} {} {} JOB'.format(reference_species, query_species, mode, similarity_threshold, line_start, line_end, 'JOB') + '\n')

				jobfile.write(script_name + '\n')

				line_start = line_end + 1
				line_end = line_start + lines_per_job - 1

				file_no += 1

		print("Made job directory at phyloV2/jobs/sim{}/{} \n".format(similarity_threshold, jobdir.split('/')[-1]))

		with open(jobdir + '/command', 'w') as para_command:
			para_command.write('#!/bin/sh' + '\n')
			para_command.write('cd {}\n'.format(jobdir))
			para_command.write("chmod +x scripts/*" + '\n')
			para_command.write("para create job -ram=16g -maxJob=100" + '\n')
			para_command.write("para push" + '\n')
		print("Wrote command.")

		subprocess.call('chmod +x {}/command'.format(jobdir), shell=True)
		print("Changed permissions.")

		subprocess.call("{}/command".format(jobdir), shell=True)
		print("Submitted command.")

		print("\nJobs submitted. You'll find their output in phylo/outputs.")

if __name__ == '__main__':
	main()