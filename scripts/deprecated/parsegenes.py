import pickle

stem = '/cluster/u/yatisht/for_james/'
# ids = [line.split() for line in open(stem+'hg38.genes_id', 'r').readlines()]
genechroms = pickle.load(open('/cluster/u/yatisht/for_james/mm10/genes_chr.p', 'r'))
genecoords = pickle.load(open('/cluster/u/yatisht/for_james/mm10/genes_coords.p', 'r'))
out = open('/cluster/u/jahemker/phylo2/inputs/mm10_genes.byID.bed', 'w')
genes = []

for c, _id in enumerate(genechroms.keys()):
	# transcriptID = _id[0].strip()
	transcriptID = _id

	if transcriptID not in genechroms:
		print "Chromosome not found for transcript {}".format(_id[0])
		continue

	chrom = genechroms[transcriptID]

	if transcriptID not in genecoords:
		print "Coordinates not found for transcript {}".format(transcriptID)
	allstarts = [int(pair[0]) for pair in genecoords[transcriptID]]
	allends = [int(pair[1]) for pair in genecoords[transcriptID]]

	minc = min(allstarts+allends)
	maxc = max(allstarts+allends)

	genes.append([_id, chrom, str(minc), str(maxc)])

# print(genes[:5])
genes = sorted(genes, key = lambda x: int(x[0][7:]))
print "Found chromosome and coordinates for {} genes.".format(len(genes))
out.write('\n'.join(['\t'.join(gene) for gene in genes]))
out.close()