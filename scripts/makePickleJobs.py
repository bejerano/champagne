#!/usr/bin/python3.4
# Takes a species and pickles all of its chains in separate files,
# named by chainID, for faster loading.

import os
import sys
import pickle
import subprocess
import argparse
import configparser as cp
import pdb

parser = argparse.ArgumentParser()
parser.add_argument("-np", "--no-parasol",
                    action='store_true',
                    help="Will not use the parasol system for parallelization")
parser.add_argument("-r", "--reference",
					required=True,
					help="Reference species.")
parser.add_argument("-q", "--query_species",
					required=True,
					nargs='*',
					help="Query species.")
parser.add_argument("-d", "--phylo_dir",
					required=True,
					help="Phylo directory.")
# parser.add_argument("-j", "--is_job",
# 					action='store_true')
args = parser.parse_args()

config = cp.ConfigParser(interpolation=cp.BasicInterpolation())
f = "{}/scripts/config.ini".format(args.phylo_dir)
config.read("{}/scripts/config.ini".format(args.phylo_dir))
speciesdir = config["Paths"]["speciesdir"]

species = args.query_species
ref = args.reference
jobname = 'chain-pickling-{}'.format(hash("-".join(species)))
# jobdir = "/cluster/u/jahemker/phylo2/tmp/"+jobname + '/'
jobdir = config["Paths"]["tmpdir"] + jobname + '/'
if not os.path.isdir(jobdir+'scripts/'): os.makedirs(jobdir+'scripts/')
with open(jobdir + '/pickle_chains_job', 'w') as jobfile:
	for s in species:
		scriptname = jobdir + 'scripts/job_'+str(s)+'.sh'
		with open(jobdir + '/scripts/job_'+str(s)+'.sh', 'w') as script:
			script.write('#!/bin/bash' + '\n')
			# script.write('export PYTHON_EGG_CACHE=results' + '\n')
			script.write('export PYTHON_EGG_CACHE={}'.format(config["Paths"]["cachedir"]) + '\n')
			# script.write('export PYTHONPATH=$PYTHONPATH:/cluster/u/yatisht/aligner/comparative/try/lib64/python2.7/site-packages/' + '\n')
			script.write('export PYTHONPATH=$PYTHONPATH:{}'.format(config["Paths"]["python2packages"]) + '\n')
			# script.write('python /cluster/u/jahemker/phylo2/scripts/pickleChains.py -r {} -q {} -j'.format(ref, s) + '\n')
			script.write('python {}scripts/pickleChains.py -r {} -q {} -d {}'.format(config["Paths"]["phylodir"], ref, s, config["Paths"]["phylodir"]) + '\n')
		jobfile.write(scriptname + '\n')
		if args.no_parasol:
			print("Running scriptfile {}".format(scriptname))
			subprocess.call("chmod +x {}".format(scriptname), shell=True)
			subprocess.call(scriptname, shell=True)

if not args.no_parasol:
	with open(jobdir + '/command', 'w') as para_command:
		para_command.write('#!/bin/bash' + '\n')
		para_command.write('cd {}\n'.format(jobdir))
		para_command.write("chmod +x scripts/*" + '\n')
		para_command.write("para create pickle_chains_job -ram=22g -cpu=1 -maxJob=20" + '\n')
		para_command.write("para push" + '\n')

	subprocess.call('chmod +x {}/command'.format(jobdir), shell=True)
	subprocess.call("{}/command".format(jobdir), shell=True)