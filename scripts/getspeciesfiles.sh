#!/bin/bash

para=$1
ref=$2
species=$3
phylodir=$4

if [ $# -lt 4 ]; then
    echo "You have to provide as arguments [P/NP] REFERENCE SPECIES PHYLODIR (ending without /)."
    echo "P - use parasol batch system |or| NP - do not use parasol batch system"
    echo "REFERENCE - reference assembly (hg38)"
    echo "SPECIES - query assembly (canFam3)"
    echo "PHYLODIR - same as in config"
    exit
fi

if [ $para != "P" ] && [ $para != "NP" ]; then
	echo "Your first argument must be \"P\" or \"NP\"."
	exit
fi

echo "Retrieving files for species $species..."

specdir=$phylodir/species/$species
chromdir=$phylodir/species/$species/chromseqs/
refdir=$phylodir/species/$species/ref-$ref

echo "Species directory: $specdir"
echo "Species/reference directory: $refdir"

if [ -d $chromdir ]; then
	echo "The directory for this species' chromosomal sequences already exists."
else
	echo "Making directory for chromosomal sequences."
	mkdir -p $chromdir
fi

if [ -d $refdir ]; then
	echo "The directory for this species/reference combination already exists."
else
	echo "Making directory for species/reference combination."
	mkdir -p $refdir
fi

# 1. GET CHAIN
printf "\n***** Getting chain (1). *****\n"
if [ -f $refdir/$ref.$species.all.chain ]; then 
	echo "The chain ($ref.$species.all.chain) is already in the reference/species directory."
else
	echo "Please put the chain ($ref.$species.all.chain) in the reference/species directory."
fi

# 2. GET chainIDs
printf "\n***** Getting chainIDs (2). *****\n"
if [ -f $refdir/$species.trimmed.chain_ids ]; then 
	echo "The trimmed chain ID file ($species.trimmed.chain_ids) is already in the reference/species directory."
else
	echo "Please put the trimmed chain ID file ($species.trimmed.chain_ids) in the reference/species directory."
fi

# 3. SUBMIT chain-pickling job
printf "\n***** Pickling chains (3). *****\n"
if [ -d ${refdir}/pickledChains ]; then
	echo "The pickledChains directory already appears to exist, so not submitting job."
else
	if [ -f ${refdir}/$ref.$species.all.chain ] && [ -f  ${refdir}/$species.trimmed.chain_ids ]; then
		echo "The chain and chain ID files appear to exist, so submitting chain pickling job."

		if [ $para == "P" ]; then
			pycmd=$phylodir/scripts/makePickleJobs.py
			$pycmd -r $ref -q $species -d $phylodir	
		else
			echo "NP argument detected. Not using parasol batch system. May take a very long time..."
			pycmd=$phylodir/scripts/makePickleJobs.py
			$pycmd -np -r $ref -q $species -d $phylodir	
		fi
		
	else 
		echo "The chain and chain ID files don't appear to exist, so can't pickle chains."
	fi
fi

# 4. GET SEQUENCE
printf "\n***** Getting sequence (4). *****\n"
if [ -f $specdir/$species.fa ] && [ -f $specdir/$species.2bit ] && [ -f $specdir/$species.chrom.sizes ]; then
	echo "The fasta, 2bit, and chromsizes files are already in the species' directory."
else
	echo "Please put the fasta, 2bit, and chromsizes files in the species' directory."
fi

tput bel
