#!/usr/bin/python3.4
# Takes a species and pickles all of its chains in separate files,
# named by chainID, for faster loading.

import os
import sys
import pickle
import subprocess
import argparse
import ConfigParser as cp
import pdb

parser = argparse.ArgumentParser()
parser.add_argument("-r", "--reference",
					required=True,
					help="Reference species.")
parser.add_argument("-q", "--query_species",
					required=True,
					nargs='*',
					help="Query species.")
parser.add_argument("-d", "--phylo_dir",
					required=True,
					help="Phylo directory.")
# parser.add_argument("-j", "--is_job",
# 					action='store_true')
args = parser.parse_args()

dir_path = os.path.dirname(os.path.realpath(__file__))
config = cp.SafeConfigParser()
config.read("{}/config.ini".format(dir_path))
speciesdir = config.get('Paths', 'speciesdir', 0)
# speciesdir = '/cluster/u/jahemker/phylo2/species/'
# if (args.is_job):
s = args.query_species[0]
ref = args.reference
id_file = '{}{}/ref-{}/{}.trimmed.chain_ids'.format(speciesdir, s, ref, s)
ids = sorted(list(set([line.split()[1] for line in open(id_file, 'r').readlines()])), key = lambda x:int(x))
print("There are {} chains to load.".format(len(ids)))
if not os.path.isdir('{}{}/ref-{}/pickledChains/'.format(speciesdir, s, ref)):
	os.makedirs('{}{}/ref-{}/pickledChains/'.format(speciesdir, s, ref))
inChain = False
chain = []
idIndex = 0
print("Searching for chain {}.".format(ids[idIndex]))

chainfile = '{}{}/ref-{}/{}.{}.all.chain'.format(speciesdir, s, ref, ref, s)

for line in open(chainfile, 'r').readlines():
	line = [word.strip() for word in line.split()]
	if len(line) == 0 or line[0].startswith('#'): continue

	if line[0]=='chain' and inChain is True: 
		with open('{}{}/ref-{}/pickledChains/{}.pickle'.format(speciesdir, s, ref, ids[idIndex]), 'wb') as out:
			pickle.dump(chain, out, protocol=2)
		print("Serialized chain {}, with {} lines.".format(ids[idIndex], len(chain)))
		chain = []
		idIndex += 1
		inChain=False
		if idIndex==len(ids): 
			print("Serialized all chains.")
			break
		print("\nSearching for chain {}.".format(ids[idIndex]))

	if line[0]=='chain' and line[12]==ids[idIndex]: 
		print("Found chain {}.".format(ids[idIndex]))
		inChain=True

	if inChain: chain.append(line)
# else:
# 	species = args.query_species
# 	ref = args.reference
# 	jobname = 'chain-pickling-{}'.format(hash("-".join(species)))
# 	# jobdir = "/cluster/u/jahemker/phylo2/tmp/"+jobname + '/'
# 	jobdir = config["Paths"]["tmpdir"] + jobname + '/'
# 	if not os.path.isdir(jobdir+'scripts/'): os.makedirs(jobdir+'scripts/')
# 	with open(jobdir + '/pickle_chains_job', 'w') as jobfile:
# 		for s in species:
# 			scriptname = jobdir + 'scripts/job_'+str(s)+'.sh'
# 			with open(jobdir + '/scripts/job_'+str(s)+'.sh', 'w') as script:
# 				script.write('#!/bin/bash' + '\n')
# 				script.write('export PYTHON_EGG_CACHE=results' + '\n')
# 				# script.write('export PYTHONPATH=$PYTHONPATH:/cluster/u/yatisht/aligner/comparative/try/lib64/python2.7/site-packages/' + '\n')
# 				# script.write('export PYTHONPATH=$PYTHONPATH:{}'.format(config["Paths"]["python2packages"]) + '\n')
# 				script.write('export PYTHONPATH=$PYTHONPATH:{}'.format(config["Paths"]["python3packages"]) + '\n')
# 				# script.write('python /cluster/u/jahemker/phylo2/scripts/pickleChains.py -r {} -q {} -j'.format(ref, s) + '\n')
# 				script.write('python {}scripts/pickleChains.py -r {} -q {} -d {} -j'.format(config["Paths"]["phylodir"], ref, s, config["Paths"]["phylodir"]) + '\n')
# 			jobfile.write(scriptname + '\n')

# 	with open(jobdir + '/command', 'w') as para_command:
# 		para_command.write('#!/bin/bash' + '\n')
# 		para_command.write('cd {}\n'.format(jobdir))
# 		para_command.write("chmod +x scripts/*" + '\n')
# 		para_command.write("para create pickle_chains_job -ram=22g -cpu=1 -maxJob=20" + '\n')
# 		para_command.write("para push" + '\n')

# 	subprocess.call('chmod +x {}/command'.format(jobdir), shell=True)
# 	subprocess.call("{}/command".format(jobdir), shell=True)

