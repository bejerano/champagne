#!/usr/bin/python3.4
import os, sys
import argparse
import configparser as cp
from io import StringIO
from Bio import Phylo
from collections import defaultdict
import pdb


"""
get_branch_scores.py

"""
config = cp.ConfigParser(interpolation=cp.BasicInterpolation())
config.read("scripts/config.ini")

parser = argparse.ArgumentParser()
parser.add_argument("-n", "--newick_tree",
					required=True)
parser.add_argument("-t", "--tree_name",
					required=True)
parser.add_argument("-i", "--ithreshold",
					type=float,
					nargs='?',
					const=float(config["Parameters"]["ithreshold"]),
					required=True)
parser.add_argument("-d", "--dthreshold",
					type=float,
					nargs='?',
					const=float(config["Parameters"]["dthreshold"]),
					required=True)
parser.add_argument('-o', "--outgroup",
					required=True)
parser.add_argument('-m', "--minSize",
					type=int,
					nargs='?',
					const=int(config["Parameters"]["minSize"]),
					required=True,
                    help="Minimum insertion/deletion size. Default is 50.")
args = parser.parse_args()

def main():

	# -- 0. Init params --

	newick_tree = args.newick_tree
	tree_name = args.tree_name
	ithreshold = args.ithreshold
	dthreshold = args.dthreshold
	outgroup = args.outgroup
	minSize = args.minSize

	PHYLO_DIR = config["Paths"]["phylodir"]
	tree_dir = config["Paths"]["treedir"] + tree_name + '/'
	nameMap = {line.split(':')[0].strip() : line.split(':')[1].strip() for line in open(PHYLO_DIR+'species/namemap.txt', 'r').readlines()}
	print("Calculating S and C for the following Newick tree: {}".format('\n'+newick_tree))

	# -- 1. Load evidence --

	evidence = list(set([line for line in open(tree_dir+'evidence.txt', 'r').readlines()]))
	evidence = [e.split() for e in evidence]
	
	# -- 2. Filter evidence --

	evidence_at_threshold = []
	unique_locations = []

	for case in evidence:
		statuses = case[5:]
		case_type = case[0]
		case_at_threshold = case[:5]
		unique_indel = ' '.join(case_at_threshold)
		if unique_indel in unique_locations:
			continue
		else:
			unique_locations.append(unique_indel)

		for status in statuses:
			# reference
			if ':' not in status:
				if case_type == 'deletion':
					case_at_threshold.append(status[:-1]+'-')
				else:
					case_at_threshold.append(status)
			# regular
			else:
				stat, score = status.split(':')
				
				# replace sub-threshold score with ?
				if score == '~':
					case_at_threshold.append(stat)
					continue

				score = float(score)
				if case_type == 'insertion' and score < ithreshold:
					case_at_threshold.append(stat[:-1]+'?')
				elif case_type == 'deletion' and score < dthreshold:
					case_at_threshold.append(stat[:-1]+'?')
				elif int(case[1]) < minSize: #This gets rid of any indels that are less than the specified minimum size
					case_at_threshold.append(stat[:-1]+'?')
				else:

					# Transform deletions
					if case_type == 'deletion':
						if stat[-1:] == '-':
							case_at_threshold.append(stat[:-1]+'+')
						elif stat[-1:] == '+':
							case_at_threshold.append(stat[:-1]+'-')
					else:
						case_at_threshold.append(stat)

		evidence_at_threshold.append(case_at_threshold)

	print("There are {} total cases to consider".format(len(evidence_at_threshold)))
	
	# Write this transformed evidence to a file
	with open(tree_dir+'evidence-at-{}-{}-{}.txt'.format(ithreshold, dthreshold, minSize), 'w') as out:
		for case in evidence_at_threshold:
			out.write(' '.join(case)+'\n')

	print("Saved transformed evidence at {}".format(tree_dir+'evidence-at-{}-{}-{}.txt'.format(ithreshold, dthreshold,minSize)))

# -- 3. Convert to Nexus (NEW)-- 
	nexus_evidence = defaultdict(list)
	species_list = newick_tree.replace('(', ' ').replace(')', ' ').replace(',', ' ').split()
	species_list.append(outgroup)
	ntax = 	len(species_list)

	with open('{}{}-{}-{}-{}.nex'.format(config["Paths"]["paupdir"], tree_name, ithreshold, dthreshold,minSize), 'w') as out:
		out.write('#NEXUS\n\n')
		#Write Taxa block
		out.write('\tBEGIN TAXA;\n')
		out.write('\tDIMENSIONS NTAX={};\n'.format(ntax))
		out.write('\tTAXLABELS\n')
		for s in species_list:
			out.write('\t\t\'{}\'\n'.format(s))
		out.write('\t;\n')
		out.write('\tENDBLOCK;\n')
		#write the characters block
		out.write('\tBEGIN CHARACTERS;\n')
		out.write('\tDIMENSIONS NCHAR={};\n'.format(len(evidence_at_threshold)))
		out.write('\tFORMAT DATATYPE=STANDARD MISSING=?;\n')
		out.write('\tCHARLABELS\n')
		nchar = 0
		for case in evidence_at_threshold:
			nchar += 1
			indel_type = case[0]
			#Ex: [1] 'insertion chr1 178103496 107' (chromosome, start coord, size)
			out.write('\t\t[{}] \'{} {} {} {}\'\n'.format(nchar, case[0], case[2], case[3], case[1]))
			for outcome in case[5:]:
				species, verdict = outcome[:-1], outcome[-1:]
				if indel_type == 'deletion':
					if verdict == '+': nexus_evidence[species].append('0')
					elif verdict == '-': nexus_evidence[species].append('1')
					elif verdict == '?': nexus_evidence[species].append('?')
				else:
					if verdict == '+': nexus_evidence[species].append('1')
					elif verdict == '-': nexus_evidence[species].append('0')
					elif verdict == '?': nexus_evidence[species].append('?')
		out.write('\t;\n')
		out.write('\tSTATELABELS\n')
		for i in range(nchar-1):
			out.write('\t\t{}\n'.format(i+1))
			out.write('\t\t\'absent\'\n')
			out.write('\t\t\'present\'\n')
			out.write('\t\t,\n')
		out.write('\t\t{}\n'.format(nchar))
		out.write('\t\t\'absent\'\n')
		out.write('\t\t\'present\'\n')
		out.write('\t;\n')
		#Write data matrices
		out.write('\tMATRIX\n')
		for species in species_list:
			out.write('\t\t' + species + ' ' + ''.join(nexus_evidence[species]) + '\n')
		out.write('\t;\n\tend;\n')
		out.write('\tbegin paup;\n')
		out.write('\t\toutgroup {};\n'.format(outgroup))
		out.write('\tend;\n')
		print("There were {} total characters.".format(nchar))
	print("Saved Nexus evidence at {}{}-{}-{}-{}.nex".format(config["Paths"]["paupdir"], tree_name, ithreshold, dthreshold, minSize))

	# -- Iterate over nodes and calculate scores --
	subtrees = [newick_tree]
	species = newick_tree.replace('(', ' ').replace(')', ' ').replace(',', ' ').split()
	print("All relevant species: {}".format(species))

	it = 1
	while True:
		print("\n-- Node {} --".format(it))

		# Base case
		if len(subtrees)==0:
			print("Reached leaf.")
			return

		subtree = subtrees.pop(0)
		lengthened_subtree = subtree
		for name in nameMap.keys():
			lengthened_subtree = lengthened_subtree.replace(name, nameMap[name])

		handle = StringIO(lengthened_subtree)
		tree = Phylo.read(handle, "newick")
		Phylo.draw_ascii(tree)

		# Remove outer brackets
		subtree = subtree[1:-1]

		# Split into two nodes
		A, B = None, None
		n_open, n_closed = 0, 0
		for i, char in enumerate(subtree):
			if char == '(':
				n_open += 1
			elif char == ')':
				n_closed += 1
			elif char == ',' and n_open==n_closed:
				A, B = subtree[:i], subtree[i+1:]

		A_species = A.replace('(', ' ').replace(')', ' ').replace(',', ' ').split()
		B_species = B.replace('(', ' ').replace(')', ' ').replace(',', ' ').split()
		X = [s for s in species if s not in A_species+B_species]
		print("A is {}".format(A_species))
		print("B is {}".format(B_species))
		print("X is {}".format(X))

		# Prevents single species being added
		if len(A_species)>1:
			subtrees += [A]
		if len(B_species)>1:
			subtrees += [B]

		# Calculate S
		S_cases, C_cases = [], []

		# Write the standardized cases to a Nexus file

		for case in evidence_at_threshold:
			# Maps from species name to character state
			statuses = {stat[:-1]:stat[-1] for stat in case[5:]}

			# Check if S
			in_A, in_B, notin_X, skip = False, False, False, False
			for s in A_species:
				if statuses[s] == '+':
					in_A = True
				if statuses[s] == '-':
					skip = True
			for s in B_species:
				if statuses[s] == '+':
					in_B = True
				if statuses[s] == '-':
					skip = True
			for s in X:
				if statuses[s] == '-':
					notin_X = True
				if statuses[s] == '+':
					skip = True

			if (in_A and in_B and notin_X and (not skip)): 
				S_cases.append(case)

			# Check if C
			A_closer = False
			in_A, notin_B, in_X, skip = False, False, False, False
			for s in A_species:
				if statuses[s] == '+':
					in_A = True
				if statuses[s] == '-':
					skip = True
			for s in B_species:
				if statuses[s] == '-':
					notin_B = True
				if statuses[s] == '+':
					skip = True
			for s in X:
				if statuses[s] == '+':
					in_X = True
				if statuses[s] == '-':
					skip = True
			
			if (in_A and notin_B and in_X and (not skip)): 
				A_closer = True

			B_closer = False
			notin_A, in_B, in_X, skip = False, False, False, False
			for s in A_species:
				if statuses[s] == '-':
					notin_A = True
				if statuses[s] == '+':
					skip = True
			for s in B_species:
				if statuses[s] == '+':
					in_B = True
				if statuses[s] == '-':
					skip = True
			for s in X:
				if statuses[s] == '+':
					in_X = True
				if statuses[s] == '-':
					skip = True

			if (notin_A and in_B and in_X and (not skip)): 
				B_closer = True

			assert((A_closer and B_closer) is False)
			if (A_closer or B_closer):
				C_cases.append(case)

		# Print scores
		S_cases = sorted(S_cases, key=lambda x: int(x[3]))

		S_cases = [' '.join(case) for case in S_cases]
		C_cases = [' '.join(case) for case in C_cases]
		print("\nAt this node, there were {} supporting cases and {} conflicting cases.".format(len(S_cases), len(C_cases)))
		show_cases = input("Want to see the cases?")

		if show_cases.startswith('y'):
			print("S cases:\n{}".format('\n'.join(S_cases)))
			print("C cases:\n{}".format('\n'.join(C_cases)))
		it+=1

if __name__ == '__main__':
	main()



