''''
#######################
#### searchGene.py ####
#######################

Searches a gene for insertions across N species. 
Documentation in README. 
'''

import os
import sys
import pickle
import warnings
import edlib
import ConfigParser as cp
from collections import defaultdict
from Bio import SeqIO
from utils import *
from verificationAlgorithms import *
import pdb

dir_path = os.path.dirname(os.path.realpath(__file__))
config = cp.SafeConfigParser()
config.read("{}/config.ini".format(dir_path))
PHYLO_DIR = config.get('Paths', 'phylodir', 0)
STEP_SIZE = config.getint('Parameters', 'stepSize')
MAX_INDEL_SIZE = config.getint('Parameters', 'maxSize')

# Vars used for debugging purposes
DEBUG = False
CASE = '10051438'

# Takes speciesStatus of format mm10+:0.75 and returns the species (mm10)
def speciesFromSpeciesStatus(speciesStatus):
	return speciesStatus.split(':')[0][:-1]

# Takes speciesStatus of format mm10+:0.75 and returns the sim (0.75)
def simFromSpeciesStatus(speciesStatus):
	return float(speciesStatus.split(':')[1])

# Takes speciesStatus of format mm10+:0.75 and returns the status (+)
def statusFromSpeciesStatus(speciesStatus):
	return speciesStatus.split(':')[0][-1]

# Takes a list of indels and returns the one with the most +/-
def selectEvidence(indels):
	best, bestScore = None, -1
	for indel in indels:
		speciesStatuses = [statusFromSpeciesStatus(species) for species in indel[5:]]
		score = sum([1 for s in speciesStatuses if s != '?'])
		if score>bestScore: best = indel
	return best


def clusterNearbyIndels(indels):
	print("Starting indels: {}".format(indels))
	compressedIndels = []
	currInterval = []
	currChrom, currStart = None, None

	for indel in indels:
		chrom, start = indel[2], int(indel[3])

		# Initialize
		if currChrom is None:
			currChrom, currStart = chrom, start

		distanceFromIntervalStart = start - currStart
		if distanceFromIntervalStart > 30 or chrom != currChrom:
			compressedIndels.append(selectEvidence(currInterval))
			currChrom, currStart, currInterval = chrom, start, []
		
		currInterval.append(indel)

	# Add last interval
	if len(currInterval) > 0:
		compressedIndels.append(selectEvidence(currInterval))

	print("Returning indels: {}".format(compressedIndels))
	return compressedIndels

def is_valid(status, reference): 
	if status[0] == 'insertion':
		atLeastOneDeletion, numInsertions = False, 0
		for species in status[5:]:
			if speciesFromSpeciesStatus(species)==reference: continue
			if statusFromSpeciesStatus(species)=='-': atLeastOneDeletion = True
			if statusFromSpeciesStatus(species)=='+':numInsertions+=1
		if atLeastOneDeletion and numInsertions>=2: return True

	elif status[0] == 'deletion':
		atLeastOneInsertion, numDeletions = False, 0
		for species in status[5:]:
			if speciesFromSpeciesStatus(species)==reference: continue
			if statusFromSpeciesStatus(species)=='-': numDeletions+=1
			if statusFromSpeciesStatus(species)=='+': atLeastOneInsertion = True
		if atLeastOneInsertion and numDeletions>=2: return True

	return False

def verify_indel(insertion, species, chains, sequences, chromsizes, similarityThreshold, windowSize, reference):
	print("Species passed: {}".format(species))
	indel, foundIn, insertionSize, rchrom, ris, rie, qchrom, qis, qie, qstrand = insertion
	print("This indel was found in {} at the COORDINATES {} {} {} {}.".format(foundIn, qchrom, qis, qie, qstrand))

	# Boost threshold for insertions by 10%
	if indel=='insertion':
		status = ['insertion', insertionSize, rchrom, ris, rie, reference+'-']
		# status = ['insertion', insertionSize, rchrom, ris, rie, reference+'-', foundIn+'+'+':1.0']

	elif indel=='deletion':
		status = ['deletion', insertionSize, rchrom, ris, rie, reference+'+']
		# status = ['deletion', insertionSize, rchrom, ris, rie, reference+'+', foundIn+'-'+':1.0']

	# Only include this indel as evidence if it could count as supporting or conflicting evidence
	valid = False

	seqs = {}

	# Check status of insertion in all species
	for s in sorted(species):
		print("***************************************\nChecking status of species {}\n***************************************".format(s))
		if s not in chains: 
			print("Skipping species {} because it is not in chains".format(s))
			status.append("{}?:~".format(s))
			print("***************************************\nEnding checking status of species {}\n***************************************".format(s))
			continue

		# For reference, just compare the window sequences
		if s==foundIn: 
			sim, rpre, rpost, qpre, qpost = get_window_similarity(windowSize, int(ris), int(rie), int(qis), int(qie), sequences[reference], qstrand, chromsizes[s][qchrom], sequences[s])
			sign = '+' if indel=='insertion' else '-'
			if sim < similarityThreshold:
				print("Returning because poor window similarity in {}".format(s))
				return False, None, None
			sim = round(sim, 2)
			status.append("{}{}:{}".format(s, sign, sim))
			print("***************************************\nEnding checking status of species {}\n***************************************".format(s))
			continue

		if indel=='insertion':
			# pdb.set_trace()
			present, sim, rseq, qseq = insertion_is_present(insertion, reference, sequences[reference], foundIn, sequences[foundIn], s, sequences[s], chains[s], chromsizes, similarityThreshold, windowSize)
			if present: 
				status.append("{}+:{}".format(s, sim))
				if reference not in seqs: seqs[reference] = rseq
				seqs[s] = qseq
				print("***************************************\nEnding checking status of species {}\n***************************************".format(s))
				continue
			absent, sim, rseq, qseq = insertion_is_absent(insertion, reference, sequences[reference], s, sequences[s], chains[s], similarityThreshold, windowSize)
			if absent: 
				status.append("{}-:{}".format(s, sim))
				if reference not in seqs: seqs[reference] = rseq
				seqs[s] = qseq
			else: 
				status.append("{}?:~".format(s))

		elif indel=='deletion':
			present, sim, rseq, qseq= deletion_is_present(insertion, reference, sequences[reference], s, sequences[s], chains[s], similarityThreshold, windowSize)
			if present:
				status.append("{}+:{}".format(s, sim))
				if reference not in seqs: seqs[reference] = rseq
				seqs[s] = qseq
				print("***************************************\nEnding checking status of species {}\n***************************************".format(s))
				continue
			absent, sim, rseq, qseq = deletion_is_absent(insertion, reference, sequences[reference], s, sequences[s], chains[s], similarityThreshold, windowSize)
			if absent:
				status.append("{}-:{}".format(s, sim))
				if reference not in seqs: seqs[reference] = rseq
				seqs[s] = qseq
			else:
				status.append("{}?:~".format(s))
		print("***************************************\nEnding checking status of species {}\n***************************************".format(s))


	status = status[:5] + sorted(status[5:], key = lambda x: speciesFromSpeciesStatus(x))
	print("Status: ",status)

	####################################################################################
	# Returns:
	# - status of the following format:										   		   #
	# deletion 150 chr10 11100 11250 hg38+ bosTau8-:1.0 canFam3+:0.58 susScr3-:0.65  
	# - dict mapping from species to sequences for all species with assigned states     #
	####################################################################################

	return True, status, seqs

# Slides across all chains, searching for insertions in any species
# NOTE: We are NOT currently dealing well with duplicates (we don't even really check for them)
def search_for_insertions(sites, species, chains, sequences, chromsizes, treeDir, similarityThreshold, minimumSize, windowSize, reference, geneName):
	sites = sorted(sites, key=lambda x: int(x[2]))
	gps = defaultdict(dict) 
	chroms = {}
	reachedEnd = []

	# insertions/deletions considered/accepted
	IC, IA = 0, 0
	DC, DA = 0, 0

	site_to_seqs = {}

	with open(treeDir+'evidence/{}.txt'.format(sites[0][0]), 'w') as ev:
		with open(treeDir+'negatives/{}.txt'.format(sites[0][0]), 'w') as negs:
			inIntron = False
			reachedEndOfAllChains = False
			nextIntron = 0
			usableInsertions, usableDeletions = [], []

			# Initialize chain locations
			chroms[reference] = sites[0][1]
			print(sites[0])
			sequences[reference] = load_sequence(reference, chroms[reference], treeDir, geneName)
			for s in species: 
				if s not in chains: continue
				gps[s] = {'rc': int(chains[s][0][5]), #start coord of alignment in reference species (update to become current coord)
						  'qc': int(chains[s][0][10]), #start coord of alignment in query species (update to become current coord)
						  'strand': chains[s][0][9], #query sequence strand
					      'lineNum': 1, #which line are we reading from in the chain file
					      'col': 0, #which column we are in 
					      'bpLeft': int(chains[s][1][0]) #how many bp are left in this column before we have to change column
					      }
				print("{}: {}".format(s, ', '.join([k + ': ' +str(gps[s][k]) for k in gps[s].keys()])))
				chroms[s] = chains[s][0][7]

			start_coords = [gps[s]['rc'] for s in species if s in chains]
			if len(start_coords)==0: return
			rcc = min(start_coords)

			if s not in gps: print("keys in chains: {}".format(' '.join(list(chains.keys()))))

			print("\nProceeding with search.")	
			aggregate_insertions = []	

			# Search until we have searched all introns		
			while nextIntron < len(sites):
				if reachedEndOfAllChains: break

				indels = []
				rcc += STEP_SIZE

				# Check if we're in an intron
				if int(sites[nextIntron][2]) <= rcc <= int(sites[nextIntron][3]) and inIntron is False: 
					print("\n------- Entering range: region {} out of {}. -------".format(nextIntron+1,len(sites)))
					inIntron = True
				if inIntron is True and rcc >= int(sites[nextIntron][3]): 
					print('------- Leaving range. --------')
					inIntron = False
					nextIntron += 1
					while nextIntron < len(sites) and int(sites[nextIntron][3]) <= rcc: nextIntron += 1

				# Update positions in all chains
				#In each species we take a step
				for s in gps.keys():
					if s in reachedEnd: 
						print("Reached end of {} chain. Skipping.".format(s))
						continue
					if len(chains[s][gps[s]['lineNum']]) < 3:
						print("Appending.", reachedEnd) 
						reachedEnd.append(s)
						if len(reachedEnd) == len(list(chains.keys())): 
							print("Reached end of all chains!")
							reachedEndOfAllChains = True
							break
						continue
					

					prev = gps[s]['rc']
					gps[s]['rc'] = max(gps[s]['rc'], rcc)

					if gps[s]['rc'] == rcc and prev != rcc - STEP_SIZE: #detects when we enter the chain
						gps[s]['qc'] = gps[s]['qc'] + (rcc-prev)
						print("We've entered the range of the {} chain.".format(s))

					# If we've reached the start of this chain, update properties
					# NOTE: do we need to add  prev != rcc - STEP_SIZE in the if statement here as well
					if gps[s]['rc'] == rcc:
						increment = gps[s]['bpLeft']
						gps[s]['bpLeft'] = gps[s]['bpLeft'] - STEP_SIZE
						changedLine = False


						#If we are still in an ungapped aligment, we have taken 10 bp step in the ref coord
						#we need to mirror that step in the query coord and take 10 bp
						if gps[s]['col'] == 0 and gps[s]['bpLeft'] > 0: gps[s]['qc'] += STEP_SIZE

						# Have to enter next col/line if we run out of ungapped alignment
						while gps[s]['bpLeft'] <= 0:
							if gps[s]['col'] == 0:
								gps[s]['col'] = 1
								gps[s]['qc'] += increment

							# If we were in a query gap and there are no bp left, then we change lines and are
							# back into an ungapped alignment
							elif gps[s]['col'] == 1:
								gps[s]['col'] = 0
								gps[s]['lineNum'] += 1
								changedLine = True
								#This is important because we will want to check to see if those gaps were indels

								# Update qc to account to rgap 
								gps[s]['qc'] += int(chains[s][gps[s]['lineNum']-1][2])

							#Takes into account the fact that we stepped into the next ungapped alignment for some distance
							#Note: put in print states to figure out which object is throwing the error
							# len(chains[s]) <= gps[s]['lineNum']



							#This checks to see if we are at the end of a chain file
							if len(chains[s][gps[s]['lineNum']]) < 3: 
								reachedEnd.append(s)
								print("Reached end of {} chain!!!!!!!! Skipping.".format(s))
								break
								
							# print("Length of chains[s]: {}".format(len(chains[s])))
							# print("gps[s][linenum]: {}".format(gps[s]['lineNum']))
							# print("gps[s][col]: {}".format(gps[s]['col']))
							# print("chains[s][gps[s]['lineNum']]: {}".format(chains[s][gps[s]['lineNum']]))
							newBpLeft = int(chains[s][gps[s]['lineNum']][gps[s]['col']]) + gps[s]['bpLeft']
							increment =  newBpLeft - gps[s]['bpLeft'] 
							gps[s]['bpLeft'] = newBpLeft

							if gps[s]['col'] == 0 and gps[s]['bpLeft'] > 0: gps[s]['qc'] += (int(chains[s][gps[s]['lineNum']][0])-gps[s]['bpLeft'])

						# Check for insertion
						if (changedLine is True and inIntron is True 
							and int(chains[s][gps[s]['lineNum']-1][1]) == 0
							and int(chains[s][gps[s]['lineNum']-1][2]) >= minimumSize):

							insertionSize, overstep = int(chains[s][gps[s]['lineNum']-1][2]), int(chains[s][gps[s]['lineNum']][0]) - gps[s]['bpLeft']
							rip, qchrom, qis, qie, qstrand = rcc-overstep, chroms[s], gps[s]['qc']-overstep-insertionSize, gps[s]['qc']-overstep, gps[s]['strand']
							
							insertionSeq = get_seq_from_browser_coords(qis, qie, qstrand, chromsizes[s][qchrom], sequences[s])

							indels.append(['insertion', s, str(insertionSize), chroms[reference], str(rip), str(rip), qchrom, str(qis), str(qie), qstrand])
							# aggregate_insertions.append([s, str(insertionSize), chroms[reference], str(rip), qchrom, str(qis), str(qie), qstrand])

						# Check for shared deletion
						if (changedLine is True and inIntron is True
							and int(chains[s][gps[s]['lineNum']-1][2]) == 0
							and int(chains[s][gps[s]['lineNum']-1][1]) >= minimumSize):

							deletionSize, overstep = int(chains[s][gps[s]['lineNum']-1][1]), int(chains[s][gps[s]['lineNum']][0]) - gps[s]['bpLeft']
							ris, rie, qchrom, qip, qstrand = rcc-overstep-deletionSize, rcc-overstep, chroms[s], gps[s]['qc']-overstep, gps[s]['strand']
							
							deletionSeq = get_seq_from_browser_coords(ris, rie, '+', -1, sequences[reference])
							# print("Deletion seq: {}".format(deletionSeq))

							indels.append(['deletion', s, str(deletionSize), chroms[reference], str(ris), str(rie), qchrom, str(qip), str(qip), qstrand])
							# aggregate_insertions.append([s, str(insertionSize), chroms[reference], str(rip), qchrom, str(qis), str(qie), qstrand])

				# We actually search every indel that we find: we filter them later

				for indel in indels:

					# If in debug mode, only search the desired site
					if not DEBUG or (DEBUG and CASE in indel):
						if int(indel[2]) > MAX_INDEL_SIZE:
							print('----- Skipping {} of size {} at hg38 coordinates {}... -----'.format(indel[0], indel[2], " ".join(indel[3:6])))
							print("*--------------------------------------------*")
							continue
						print("*----- Verifying {} of size {} at hg38 coordinates {}... -----*".format(indel[0], indel[2], " ".join(indel[3:6])))
						
						# --- Print out reference window if insertion --- #
						if indel[0] == 'insertion':
							IC += 1
					
							rip = int(indel[4])
							if int(indel[4]) != int(indel[5]): print("THERE's A BUG")
							rprewindow = str(sequences[reference][rip-windowSize:rip].seq).lower()
							rpostwindow = str(sequences[reference][rip:rip+windowSize].seq).lower()
							print("OUTGROUP PREWINDOW: {}".format(rprewindow))
							print("OUTGROUP POSTWINDOW: {}".format(rpostwindow))
						else: 
							DC += 1

						# # INSERTION WINDOW BOOSTING: THIS IS TRIAL, a
						# if indel[0] == 'insertion':
						# 	adjustedWindowSize = windowSize + 50
						# else:
						# 	adjustedWindowSize = windowSize
						adjustedWindowSize = windowSize

						keep, status, seqs = verify_indel(indel, species, chains, sequences, chromsizes, similarityThreshold, adjustedWindowSize, reference)
						if not keep: continue
						print("\nSTATUS: \n{}\n".format(" ".join(status)))

						isValidEvidence = is_valid(status, reference)

						# NOTE: We only write insertion sequences to pickled dict
						print("isValidEvidence: ",isValidEvidence)
						if isValidEvidence:
							if indel[0] == 'insertion':

								# Add query1 window sequence to dictionary
								q1 = indel[1]
								q1chrom, q1s, q1e, q1strand = indel[6:]
								q1s, q1e = int(q1s), int(q1e)
								q1_prewindow = get_seq_from_browser_coords(q1s-adjustedWindowSize, q1s, q1strand, chromsizes[q1][q1chrom], sequences[q1])
								insertionSeq = get_seq_from_browser_coords(q1s, q1e, q1strand, chromsizes[q1][q1chrom], sequences[q1])
								q1_postwindow = get_seq_from_browser_coords(q1e, q1e+adjustedWindowSize, q1strand, chromsizes[q1][q1chrom], sequences[q1])
								seqs[q1] = q1_prewindow + insertionSeq + q1_postwindow

								site_to_seqs[" ".join(status)] = seqs
								usableInsertions.append(status)
								IA += 1
							else:

								# Add query1 window sequence to dictionary
								q1 = indel[1]
								q1chrom, q1s, q1e, q1strand = indel[6:]
								qip = int(q1s)
								q1_prewindow = get_seq_from_browser_coords(qip-adjustedWindowSize, qip, q1strand, chromsizes[q1][q1chrom], sequences[q1])
								q1_postwindow = get_seq_from_browser_coords(qip, qip+adjustedWindowSize, q1strand, chromsizes[q1][q1chrom], sequences[q1])
								seqs[q1] = q1_prewindow + q1_postwindow

								site_to_seqs[" ".join(status)] = seqs
								usableDeletions.append(status)
								DA += 1
						else:
							negs.write(" ".join(status) + '\n')
						print("*--------------------------------------------*")

						if DEBUG: return

			# Filter out duplicates
			# Sort on chromosome, then start position
			usableInsertions = clusterNearbyIndels(sorted(usableInsertions, key = lambda x: (x[2][3:], int(x[3]))))
			usableDeletions = clusterNearbyIndels(sorted(usableDeletions, key = lambda x: (x[2][3:], int(x[3]))))
			
			for status in usableInsertions + usableDeletions: ev.write(" ".join(status) + '\n')

		if len(list(site_to_seqs.keys())) > 0:
			pickle.dump(site_to_seqs, open(treeDir + 'MSA/{}.pkl'.format(geneName), 'wb'))
		print("\nIC vs. IA: (insertions considered/accepted): {}/{} -> {}.".format(IC, IA, round(IA/(IC+1), 2)))
		print("DC vs. DA: (deletions considered/accepted): {}/{} -> {}.".format(DC, DA, round(DA/(DC+1), 2)))
		print ("Search completed.")

	return

# Loads and returns a chain, formatted as a list of split
# chain lines
def load_chain(reference, species, chainID):
	pickled_chain = PHYLO_DIR + 'species/{}/ref-{}/pickledChains/{}.pickle'.format(species, reference, chainID)
	return pickle.load(open(pickled_chain, 'rb'))

def load_sequence(species, chrom, treeDir, geneName):
	fastafile = PHYLO_DIR + 'species/{}/chromseqs/{}.fa'.format(species, chrom)
	if not os.path.exists(fastafile): 
		print("The {}.fa sequence file doesn't actually exist in the {}'s chromseqs directory.".format(chrom, species))
		with open(treeDir + 'failed-no-sequence/{}'.format(geneName), 'w') as f: f.write("Failed.")
		exit(0)
	return SeqIO.read(fastafile, 'fasta')

def load_chromsizes(species):
	sizesfile = PHYLO_DIR + 'species/{}/{}.chrom.sizes'.format(species, species)
	sizes = {}
	for line in open(sizesfile, 'r').readlines():
		line = line.split()
		sizes[line[0]] = int(line[1])
	return sizes

def main():

	# To filter egg-cache warnings
	warnings.simplefilter("ignore")

	argv = sys.argv
	treeName = argv[1]
	geneName = argv[2]
	similarityThreshold = float(argv[3])
	minimumSize = int(argv[4])
	windowSize = int(argv[5])
	reference = argv[6]
	treeDir = PHYLO_DIR + 'trees/{}/'.format(treeName)
	species = [line.strip() for line in open(treeDir+"summary.txt", 'r').readlines()[1:]]
	sites = [line.split() for line in open(treeDir+"genes/{}.txt".format(geneName), 'r')]
	chainIDs = {species[c]: _id for c, _id in enumerate(sites[0][4:])}

	
	sys.stdout = open(treeDir+'log/{}.txt'.format(geneName), 'w')

	print("Searching the following sites: ")
	print("\n".join([' '.join(site) for site in sites]))

	chains = {}
	sequences = {}
	chromsizes = defaultdict(dict)

	# 1. Load chains
	for c, s in enumerate(sorted(species)):
		print("Loading chain {} for {}.".format(chainIDs[s], s))
		if chainIDs[s] == 'NULL': 
			print("There was no matching chain.")
			continue
		chains[s] = load_chain(reference, s, chainIDs[s])
		print("First two lines of chain: {}".format('\n'.join([' '.join(line) for line in chains[s][:2]])))

	# 2. Load sequences
	for c, s in enumerate(species):
		if chainIDs[s] == 'NULL': continue
		chainChrom = chains[s][0][7]
		print("Loading sequence for {}, chromosome {}.".format(s, chainChrom))
		sequences[s] = load_sequence(s, chainChrom, treeDir, geneName)
		chromsizes[s] = load_chromsizes(s)

	search_for_insertions(sites, species, chains, sequences, chromsizes, treeDir, similarityThreshold, minimumSize, windowSize, reference, geneName)

	return

if __name__ == '__main__':
	main()