#####################
###    utils.py   ###
#####################
#
# Contains utility functions that are used in both step 1 and step 2
# ------------------------------------------------------------------

import time
import os
import parasail
import ConfigParser as cp
from collections import defaultdict


dir_path = os.path.dirname(os.path.realpath(__file__))
config = cp.SafeConfigParser()
config.read("{}/config.ini".format(dir_path))
MAX_INDEL_SIZE = config.getint('Parameters', 'maxSize')

# Uses UCSC genome browser coordinates to extract the correct sequence,
# accounting for change of strand. 
def get_seq_from_browser_coords(start, end, strand, chromsize, chromseq):
	seqstart = chromsize - end if strand=='-' else start 
	seqend = chromsize - start if strand=='-' else end

	if strand=='-':
		seq = str(chromseq[seqstart:seqend].reverse_complement().seq).lower()
	else:
		seq = str(chromseq[seqstart:seqend].seq).lower()

	return seq 

def compare_seqs(rseq, qseq):
	len_longer_seq = max([len(rseq), len(qseq)])
	print("Total rseq: {}".format(rseq))
	print("Total qseq: {}".format(qseq))
	hoxd55_path = '{}hoxd55.txt'.format(config.get('Paths', 'inputsdir', 0))
	matrix = parasail.Matrix(hoxd55_path)
	if len(rseq) == 0 or len(qseq) == 0: return 0
	result = parasail.sg_trace(rseq, qseq, 400, 30, matrix)
	s = str(result.cigar.decode).strip()
	matches = 0
	for i, char in enumerate(s):
		if char=='=':
			j = i-1
			num = ''
			while s[j].isdigit(): 
				num = s[j] + num
				j -= 1
			if num=='': print('num is empty!')
			matches += int(num)

	print('Matches: {}\n'.format(matches)) 
	
	return float(matches)/len_longer_seq

# July 17th 2019: this now returns the MINIMUM similarity out of left, right window
# Takes ref_region_start, ref_region_end, query_region_start, query_region_end,
# query_strand, query_chrom_size, and query_chrom_sequence
def get_window_similarity(windowsz, rs, re, qs, qe, rchromseq, qstrand, qchromsize, qchromseq):
	
	rprewindow = str(rchromseq[rs-windowsz:rs].seq).lower()
	rpostwindow = str(rchromseq[re:re+windowsz].seq).lower()
	rwindow = rprewindow+rpostwindow
	print("RPRE: {}".format(rprewindow))
	print("RPOST: {}".format(rpostwindow))

	qprewindow = get_seq_from_browser_coords(qs-windowsz, qs, qstrand, qchromsize, qchromseq)
	qpostwindow = get_seq_from_browser_coords(qe, qe+windowsz, qstrand, qchromsize, qchromseq)
	qwindow = qprewindow+qpostwindow
	print("QPRE: {}".format(qprewindow))
	print("QPOST: {}".format(qpostwindow))

	left_score = compare_seqs(rprewindow, qprewindow)
	right_score = compare_seqs(rpostwindow, qpostwindow)
	
	return min(left_score, right_score), rprewindow, rpostwindow, qprewindow, qpostwindow


def convert_coords(chain_start, chain_end, strand, chrom_size):
	forward_start = chrom_size - chain_end if strand == '-' else chain_start
	forward_end = chrom_size - chain_start if strand == '-' else chain_end
	return forward_start, forward_end


def get_similarity(rseq, qchromseq, qstrand, qchromsize, qstart, qend):
	if qstart == None or qend == None or qstart==qend: 
		print("Returning none from get_parasail_similarity(): qs={}, qe={}".format(qstart, qend))
		return 0, None, None
	qseq = get_seq_from_browser_coords(qstart, qend, qstrand, qchromsize, qchromseq)
	# print("{} {} {}".format(qstart, qend, qstrand))
	len_longer_seq = max([len(rseq), len(qseq)])
	if len_longer_seq > MAX_INDEL_SIZE:
		return 0, None, None
	return compare_seqs(rseq, qseq), rseq, qseq

# @param sitesfile: file containing 
# @param start : first line of sitesfile to look at (inclusive)
# @param end : last line of sitesfile to look at (inclusive)
def get_sites_dict(sitesfile, start, end, mode):

	print("Loading sites.")
	sites_dict = defaultdict(list)

	# For step 2 output files
	col = 9

	with open(sitesfile, 'r') as f:
		for lineNum, line in enumerate(f.readlines(), 1):
			if lineNum < start: continue
			if lineNum > end: break
			line = line.split()
			sites_dict[line[col]].append(line)

	print("Sites loaded. \n")
	return sites_dict

# Returns dictionary with (chainID : list of lines in chain) items
#
# @ param chainfile: file containing chain
# @ param sites_dict : dictionary containing (chainID : )
def get_chain_dict(chainfile, sites_dict):

	print("Loading chains.")
	begin_chainload = time.time()

	chain_dict = defaultdict(list)
	chainIDs = sites_dict.keys()
	maxID = str(max([int(chainID) for chainID in chainIDs]))
	minID = str(min([int(chainID) for chainID in chainIDs]))

	foundFirstChain = False
	loadedAllChains = False

	with open(chainfile, 'r') as f:
		curr_chain_id = -1
		curr_chain = []
		num_chains = 0

		for line in f.readlines():
			line = [word.strip() for word in line.split()]

			# ignore comments and blank line at end of each chain
			if len(line) == 0 or line[0].startswith('#'): continue 

			################ Deal with line ################
			# Only start building dict once reached min chainID
			if foundFirstChain == False: 
				if line[0] == 'chain' and line[12] == minID:
					foundFirstChain = True
					curr_chain_id = line[12]
				else:
					continue
			else:
				if line[0] == 'chain':
					chain_dict[curr_chain_id] = curr_chain
					curr_chain = []

					# If we've reached designated limit, open new file
					if curr_chain_id == maxID:
						loadedAllChains = True
						break			

					curr_chain_id = line[12]

			curr_chain.append(line)
			################ Move to next line ################

		# Edge case: maxID is last ID in file
		if not loadedAllChains: chain_dict[curr_chain_id] = curr_chain

	print("Loaded chains in {} minutes.\n".format((time.time() - begin_chainload)/60))
	return chain_dict
