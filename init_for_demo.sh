#!/bin/bash
speciesdir=$1

echo "Downloading hg38 files into ${speciesdir} ..."
cd ${speciesdir}/hg38/
echo "Retrieving hg38.2bit from UCSC GoldenPath ..."
wget https://hgdownload.cse.ucsc.edu/goldenPath/hg38/bigZips/hg38.2bit
echo "Retrieving hg38.fa.gz from UCSC GoldenPath ..."
wget https://hgdownload.cse.ucsc.edu/goldenPath/hg38/bigZips/hg38.fa.gz
echo "Unzipping hg38.fa.gz"
gunzip hg38.fa.gz
echo "Retrieving hg38.chrom.sizes from UCSC GoldenPath ..."
wget https://hgdownload.cse.ucsc.edu/goldenPath/hg38/bigZips/hg38.chrom.sizes
echo "All hg38 files from UCSC downloaded."
echo "*** Do not forget to split hg38.fa per the README. ***"

echo "Downloading felCat8 files into ${speciesdir} ..."
cd ${speciesdir}/felCat8/
echo "Retrieving felCat8.2bit from UCSC GoldenPath ..."
wget https://hgdownload.cse.ucsc.edu/goldenPath/felCat8/bigZips/felCat8.2bit
echo "Retrieving felCat8.fa.gz from UCSC GoldenPath ..."
wget https://hgdownload.cse.ucsc.edu/goldenPath/felCat8/bigZips/felCat8.fa.gz
echo "Unzipping felCat8.fa.gz"
gunzip felCat8.fa.gz
echo "Retrieving felCat8.chrom.sizes from UCSC GoldenPath ..."
wget https://hgdownload.cse.ucsc.edu/goldenPath/felCat8/bigZips/felCat8.chrom.sizes
cd ${speciesdir}/felCat8/ref-hg38/
echo "Retrieving hg38.felCat8.all.chain.gz from UCSC GoldenPath ..."
wget https://hgdownload.cse.ucsc.edu/goldenPath/hg38/vsFelCat8/hg38.felCat8.all.chain.gz
echo "Unzipping hg38.felCat8.all.chain.gz ..."
gunzip hg38.felCat8.all.chain.gz
echo "All felCat8 files from UCSC downloaded."
echo "*** Do not forget to split felCat8.fa per the README. ***"
echo "*** Do not forget to run getspeciesfiles.sh per the README. ***"

echo "Downloading canFam3 files into ${speciesdir} ..."
cd ${speciesdir}/canFam3/
echo "Retrieving canFam3.2bit from UCSC GoldenPath ..."
wget https://hgdownload.cse.ucsc.edu/goldenPath/canFam3/bigZips/canFam3.2bit
echo "Retrieving canFam3.fa.gz from UCSC GoldenPath ..."
wget https://hgdownload.cse.ucsc.edu/goldenPath/canFam3/bigZips/canFam3.fa.gz
echo "Unzipping canFam3.fa.gz"
gunzip canFam3.fa.gz
echo "Retrieving canFam3.chrom.sizes from UCSC GoldenPath ..."
wget https://hgdownload.cse.ucsc.edu/goldenPath/canFam3/bigZips/canFam3.chrom.sizes
cd ${speciesdir}/canFam3/ref-hg38/
echo "Retrieving hg38.canFam3.all.chain.gz from UCSC GoldenPath ..."
wget https://hgdownload.cse.ucsc.edu/goldenPath/hg38/vsCanFam3/hg38.canFam3.all.chain.gz
echo "Unzipping hg38.canFam3.all.chain.gz ..."
gunzip hg38.canFam3.all.chain.gz
echo "All canFam3 files from UCSC downloaded."
echo "*** Do not forget to split canFam3.fa per the README. ***"
echo "*** Do not forget to run getspeciesfiles.sh per the README. ***"

echo "Downloading susScr3 files into ${speciesdir} ..."
cd ${speciesdir}/susScr3/
echo "Retrieving susScr3.2bit from UCSC GoldenPath ..."
wget https://hgdownload.cse.ucsc.edu/goldenPath/susScr3/bigZips/susScr3.2bit
echo "Retrieving susScr3.fa.gz from UCSC GoldenPath ..."
wget https://hgdownload.cse.ucsc.edu/goldenPath/susScr3/bigZips/susScr3.fa.gz
echo "Unzipping susScr3.fa.gz"
gunzip susScr3.fa.gz
echo "Retrieving susScr3.chrom.sizes from UCSC GoldenPath ..."
wget https://hgdownload.cse.ucsc.edu/goldenPath/susScr3/bigZips/susScr3.chrom.sizes
cd ${speciesdir}/susScr3/ref-hg38/
echo "Retrieving hg38.susScr3.all.chain.gz from UCSC GoldenPath ..."
wget https://hgdownload.cse.ucsc.edu/goldenPath/hg38/vsSusScr3/hg38.susScr3.all.chain.gz
echo "Unzipping hg38.susScr3.all.chain.gz ..."
gunzip hg38.susScr3.all.chain.gz
echo "All susScr3 files from UCSC downloaded."
echo "*** Do not forget to split susScr3.fa per the README. ***"
echo "*** Do not forget to run getspeciesfiles.sh per the README. ***"